pipeline {
  agent {
    kubernetes {
      label UUID.randomUUID().toString()
      containerTemplate {
        name 'this'
        image 'hashicorp/terraform:0.11.11'
        ttyEnabled true
        command 'cat'
      }
    }
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '30'))
  }

  environment {
    DEPLOY_TARGET = "git@bitbucket.org:staywell_core/env-provata-us-dev.git"
  }

  stages {
    // this stage serves as a minor validation for module repositories
    stage('Initialize') {
      steps {
        container('this') {
          sh 'terraform init'
        }
      }
    }
    stage('Deploy') {
      when {
        branch "develop"
      }
      steps {
        container('this') {
          // load known_hosts file for ssh work
          configFileProvider([configFile(fileId: 'known_hosts', variable: 'KNOWN_HOSTS')]) {
            sh 'su -c "mkdir -p /root/.ssh" && su -c "chmod 700 /root/.ssh"'
            sh 'su -c "cp ${KNOWN_HOSTS} /root/.ssh/known_hosts" && su -c "chmod 644 /root/.ssh/known_hosts"'
          }
          sshagent (credentials: ['ssh.bitbucket']) {
            sh '''
              module_version=$(git rev-parse --short HEAD)
              git clone -b master $DEPLOY_TARGET .deploy
              cd .deploy

              # set git user info
              git config user.email "devops@staywell.com"
              git config user.name "devops_staywell"

              # find and replace MODULE_VERSION
              sed -i -e "s|.git?ref=.*|.git?ref=$module_version\\"|" main.tf
              git commit -m "set module version to: $module_version" main.tf
              for i in 1 2 3 4 5; do git push && break || git pull --rebase; done
            '''
          }
        }
      }
    }
  }
}
