# terraform-aws-provata

Terraform module defining all resources needed to build a fully functional My Staywell environment.

## Post Deploy Steps

When a new environment is created there are a few manual one-time actions that are needed before the environment will be fully operational:

1. The microservices are not capable of initializing their databases from an empty state. Therefore we must execute the following Jenkins job in order to initialize the databases:

    * <https://jenkins.staywell.com/job/tools/job/provata-db-init/job/master/>
    * SCRIPT=rebuild.sh - this script completely destroys and rebuilds the content of the database, so be sure you are pointing at the correct endpoint for your environment
    * TARGET_HOST can be found in RDS after your environment is built

2. AWS Secrets Manager named \<env\>-external must be manually populate through the AWS console.

3. S3 bucket named \<env\>-certs must have a private SAML cert loaded to it. Currently Staywell only uses two static certs, one for all prod environments, and one for all non-prod environments. The proper cert can be retrieved from another working environment.

    * Note: this cert process is sub-optimal and will likely be changed in the near future

4. Submit a request to have internal staywell.com DNS entries created

    * Example Request: <https://stay-well.atlassian.net/browse/IS-1610>
    * This step is only applicable if the new environment is using a staywell.com domain name

5. Set new credentials for the default user accounts:

    * Two accounts are created by default for a new environment:
        * test@staywell.com
        * test2@staywell.com
    * By default, the password for both accounts is set to: password1
    * Log in to both accounts with the above information, set their passwords to new secure values, and store the new passwords in LastPass under the folder: Shared-Provata Logins

## Environments

If this list is ever out of date you can refer to Bitbucket for the current state:

* <https://bitbucket.org/dashboard/repositories?projectOwner=staywell_core&projectKey=SD&search=env-provata>

### Legacy Test

This is the original Provata testing environment that was deployed using a combination of Chef and manual configuration to the Provata AWS account. This environment is superceded by the new Dev and QA environments built by Terraform, but it will remain active until Legacy Prod is migrated to a Terraformed production environment.

* <https://osi.test.hthu.com/public>

### Legacy Prod

This is the original Provata production environment that that was deployed using a combination of Chef and manual configuration to the Provata AWS account. This environment is still the primary production My Staywell envrionment. This environment is planned to be migrated to a new Terraform built production environment and then subsequently decommissioned.

* <https://osi.hthu.com/public>
* <https://bean.hthu.com/public>

### Dev

* <https://admin.provata-us-dev.devops-staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-dev>

### Unstable

Experimental environment primarily used by the architecture team

* <https://admin.provata-us-unstable.devops-staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-unstable>

### QA

General purpose QA environment for certifying release candidates. Contact the QA team for more information on the specific usage of each QA environment.

* <https://admin.provata-us-qa.devops-staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-qa>

### North

Dedicated QA environments for validating work from the Team in the North scrum team

* <https://admin.provata-us-north.devops-staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-north>
* <https://admin.provata-us-north2.devops-staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-north2>

* <https://admin.provata-us-north2.devops-staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-north2>

### Treb

Dedicated QA environments for validating work from the Trebuchet scrum team

* <https://admin.provata-us-treb.devops-staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-treb>
* <https://admin.provata-us-treb2.devops-staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-treb2>

* <https://admin.provata-us-treb2.devops-staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-treb2>

### Piper

Dedicated QA environments for validating work from the Pied Piper scrum team

* <https://admin.provata-us-piper.devops-staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-piper>
* <https://admin.provata-us-piper2.devops-staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-piper2>


* <https://admin.provata-us-piper2.devops-staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-piper2>

### Sparta

Dedicated QA environments for validating work from the Sparta scrum team

* <https://admin.provata-us-sparta.devops-staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-sparta>
* <https://admin.provata-us-sparta2.devops-staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-sparta2>

* <https://admin.provata-us-sparta2.devops-staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-sparta2>

### UAT

Originally deployed as the Baird environment, this environment has been rebranded as our multi-tenant UAT environment.

* <https://admin.hthu-uat.staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-us-baird>

### Prod

This environment has not been built yet, but it is planned to be the replacement for Legacy Prod. Once completed, Legacy Prod and Legacy Test will be decommissioned leaving us with only enviornments that are built by Terraform using this module.

* Planned URL: <https://admin.my.staywell.com/public>

### Canada UAT

* <https://admin.ca.uat.my.staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-ca-uat>

### Canada Prod

* <https://admin.ca.my.staywell.com/public>
* <https://bitbucket.org/staywell_core/env-provata-ca-prod>

## Manual Legacy Deployments

Until Legacy Test and Legacy Prod are fully migrated to Terraform we will perform updates to them directly through the AWS Console.

1. Log in or role switch to the Provata AWS account

2. Navigate to the appropriate task definition

    * Example: <https://us-west-2.console.aws.amazon.com/ecs/home?region=us-west-2#/taskDefinitions/hthu-auth-test/status/ACTIVE>

3. Click the latest version of the selected task definition

4. Click "Create new revision"

5. Scroll down and click on the container name which will be formatted like a hyperlink

6. Update the "Image" field with the proper version number

7. Click "Update" in the bottom right of the screen

8. Scroll down and click "Create"

9. Navigate to the corresponding ECS service

    * Example: <https://us-west-2.console.aws.amazon.com/ecs/home?region=us-west-2#/clusters/provata-test/services/hthu-auth/details>

10. Click "Update" in the top right corner of the screen

11. Select the latest task definition version in the "Revision" dropdown box

12. Scroll down and click "Next step"

13. On the next screen, click "Next step" again

14. On the next screen, click "Next step" again

15. On the final screen, scroll down and click "Update Service"

At this point ECS will automatically perform a rolling update until all running tasks are updated. You can watch this process by navigating to the "Events" tab in the service status page. You will know that the update is complete when when the "Events" tab displays the message: "service \<service-name\> has reached a steady state."

Example: <https://us-west-2.console.aws.amazon.com/ecs/home?region=us-west-2#/clusters/provata-test/services/hthu-auth/events>
