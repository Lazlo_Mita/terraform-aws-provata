#################################################
# *.${var.subdomain_name}.${var.domain_name}
#################################################

resource "aws_acm_certificate" "star_subdomain" {
  domain_name       = "*.${var.subdomain_name}.${var.domain_name}"
  validation_method = "DNS"
  tags              = "${var.tags}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "star_subdomain" {
  certificate_arn         = "${aws_acm_certificate.star_subdomain.arn}"
  validation_record_fqdns = ["${aws_route53_record.star_subdomain.fqdn}"]
}

resource "aws_route53_record" "star_subdomain" {
  name    = "${aws_acm_certificate.star_subdomain.domain_validation_options.0.resource_record_name}"
  type    = "${aws_acm_certificate.star_subdomain.domain_validation_options.0.resource_record_type}"
  zone_id = "${data.aws_route53_zone.public.zone_id}"
  records = ["${aws_acm_certificate.star_subdomain.domain_validation_options.0.resource_record_value}"]
  ttl     = 60
}
