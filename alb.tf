#################################################
# ALBs
#################################################

resource "aws_lb" "hthu" {
  name               = "${var.env}-hthu"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.alb.id}"]
  subnets            = ["${data.aws_subnet_ids.public.ids}"]
  tags               = "${var.tags}"

  access_logs {
    bucket  = "${aws_s3_bucket.logs.bucket}"
    prefix  = "${var.env}-alb-hthu"
    enabled = true
  }
}

resource "aws_lb" "internal" {
  name               = "${var.env}-internal"
  internal           = true
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.alb.id}"]
  subnets            = ["${data.aws_subnet_ids.public.ids}"]
  tags               = "${var.tags}"

  access_logs {
    bucket  = "${aws_s3_bucket.logs.bucket}"
    prefix  = "${var.env}-alb-internal"
    enabled = true
  }
}

#################################################
# Security Group
#################################################

resource "aws_security_group" "alb" {
  name   = "${var.env}-alb"
  vpc_id = "${data.aws_vpc.core.id}"
  tags   = "${var.tags}"
}

#################################################
# Ingress
#################################################

resource "aws_security_group_rule" "alb_ingress_80" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  security_group_id = "${aws_security_group.alb.id}"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "alb_ingress_443" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.alb.id}"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "alb_ingress_ecs" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.alb.id}"
  source_security_group_id = "${module.ecs.sg_id}"
}

#################################################
# Egress
#################################################

resource "aws_security_group_rule" "alb_egress_ecs" {
  type                     = "egress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.alb.id}"
  source_security_group_id = "${module.ecs.sg_id}"
}
