resource "aws_cloudfront_distribution" "this" {
  comment         = "${var.env}"
  price_class     = "PriceClass_200"
  enabled         = true
  is_ipv6_enabled = true
  tags            = "${var.tags}"

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  logging_config {
    bucket = "${aws_s3_bucket.logs.bucket}.s3.amazonaws.com"
    prefix = "cf-cdn-logs"
  }

  origin {
    domain_name = "${var.env}-static.${var.subdomain_name}.${var.domain_name}"
    origin_id   = "${aws_lb.hthu.dns_name}"

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "match-viewer"
      origin_ssl_protocols   = ["TLSv1.2"]
    }
  }

  default_cache_behavior {
    target_origin_id       = "${aws_lb.hthu.dns_name}"
    viewer_protocol_policy = "https-only"
    allowed_methods        = ["GET", "HEAD", "OPTIONS"]
    cached_methods         = ["GET", "HEAD", "OPTIONS"]
    min_ttl                = 0
    max_ttl                = 31536000
    default_ttl            = 86400

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}
