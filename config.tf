# locals must be used so ${var.env} can be interpolated in the definition
locals {
  s3_tags = {
    Classification = "Business"
  }

  shared_env_vars = [
    {
      name  = "REDIS_HOST_NAME"
      value = "${aws_elasticache_cluster.hthu.cache_nodes.0.address}"
    },
    {
      name  = "REDIS_PORT"
      value = "${aws_elasticache_cluster.hthu.cache_nodes.0.port}"
    },
    {
      name  = "AWS_REGION"
      value = "${var.region}"
    },
    {
      name  = "ENVIRONMENT_NAME"
      value = "${var.env}"
    },
    {
      name  = "APP_ENV"
      value = "staging"
    },
    {
      name  = "APP_DOMAIN"
      value = "${replace(aws_route53_zone.private.name, "/[.]$/", "")}"
    },
    {
      name  = "KMS_KEY_ID"
      value = "${aws_kms_key.this.arn}"
    },
    {
      name  = "DB_CONNECTIONS_MAX"
      value = "24"
    },
    {
      name  = "DB_DRIVER"
      value = "mysql"
    },
    {
      name  = "DB_HOST"
      value = "${aws_db_instance.master.address}"
    },
    {
      name  = "DB_PASSWORD"
      value = "${random_string.this.result}"
    },
    {
      name  = "DB_PORT"
      value = "${aws_db_instance.master.port}"
    },
    {
      name  = "DB_USERNAME"
      value = "${aws_db_instance.master.username}"
    },
    {
      name  = "NEW_RELIC_LICENSE_KEY"
      value = "aa3e0b87b71240ad033e40cf4d6e1c703ca2d6bb"
    },
    {
      name  = "NEW_RELIC_LOG_LEVEL"
      value = "INFO"
    },
    {
      name  = "LOG_LEVEL"
      value = "${var.log_level}"
    },
    {
      name  = "UPLOADS_BUCKET"
      value = "${aws_s3_bucket.assets.id}"
    },
    {
      name  = "RESOURCES_BUCKET"
      value = "${aws_s3_bucket.resources.id}"
    },
    {
      name  = "ADMIN_UPLOADS_BUCKET"
      value = "${aws_s3_bucket.signups.id}"
    },
    {
      name  = "SAML_CERT_AWS_BUCKET"
      value = "${aws_s3_bucket.certs.id}"
    },
    {
      name  = "AWS_ELIGIBILITY_FILE_BUCKET"
      value = "${aws_s3_bucket.eligibility_files.id}"
    },
    {
      name  = "AWS_SECRET_MANAGER_NAME"
      value = "${aws_secretsmanager_secret.this.id}"
    },
    {
      name  = "AUTH_EXPIRY_SECS"
      value = "30"
    },
    {
      name  = "CACHE_LOG_THRESHOLD"
      value = "10000"
    },
    {
      name = "DEFAULT_CUSTOMER_KEY"
      value = "${var.customer_key}"
    },
    {
      name = "DEFAULT_USERNAME"
      value = "${var.username}"
    },
    {
      name = "DEFAULT_ELIGIBILITY_GUID"
      value = "00000000-0000-0000-0000-000000000000"
    },
    {
      name = "DEFAULT_ACCOUNT_ID"
      value = "1"
    },
    {
      name = "DEFAULT_AFFILIATION_ID"
      value = "1"
    },
    {
      name = "DEFAULT_CUSTOMER_ID"
      value = "1"
    },
    {
      name = "DEFAULT_BENEFIT_YEAR_CODE"
      value = "${var.benefit_year_code}"
    }

  ]
}

variable "customer_key" {
    description = "customer key for default customer"
    default = "admin"
}

variable "username" {
    description = "username for default user"
    default = "admin"
}

variable "benefit_year_code" {
    description = "code for default customer benefit year"
    default = "admin2020"
}

variable "tags" {
  description = "(optional) Additional tags to be applied to all resources"
  default     = {}
}

variable "ssl_policy" {
  description = "https://docs.aws.amazon.com/elasticloadbalancing/latest/application/create-https-listener.html"
  default     = "ELBSecurityPolicy-TLS-1-1-2017-01"
}

variable "sftp_user_key" {
  description = "Key to look up the SFTP username value in the eligibility secret"
  default     = "hct-sftp-username"
}

variable "sftp_pass_key" {
  description = "Key to look up the SFTP password value in the eligibility secret"
  default     = "hct-sftp-password"
}

variable "service_credentials" {
  description = "The service credentials, provided in environment variables, for the environment."
  default     = "c2VydmljZUBwcm92YXRhaGVhbHRoLmNvbTpXY2lIUHlFZDNDWXlVek5N"
}

variable "log_level" {
  description = "Sets the log level of all services"
  default     = "info"
}

variable "node_env" {
  description = "This value selects a hard coded set of config from a dictionary in hthu-web"
  default     = "envBased"
}

variable "fake_av_scan" {
  description = "Disable anti-virus while gracefully feigning the results"
  default     = "false"
}

variable "enable_av" {
  description = "Enable or disable the anti-virus suite"

  # currently this setting only works in development and will have no effect when set to false
  default = "true"
}

variable "av_public_key" {
  description = "Public key for the anti-virus suite"
  default     = "8bf4112592beb09ac13063824df42f11"
}

variable "av_secret_key" {
  description = "Secret key for the anti-virus suite"
  default     = "a15e964a9"
}

variable "smtp_from_address" {
  description = "From email address required for SMTP communication"
  default     = "no-reply@staywell.com"
}

variable "java_timezone" {
  description = "https://garygregory.wordpress.com/2013/06/18/what-are-the-java-timezone-ids/"
  default     = "America/Chicago"
}

variable "smarty_streets_auth_id" {
  description = "The smarty streets auth id, provided in environment variables, for the environment."
  default     = "e2180e1e-0ac2-4220-980f-b1542ebb2a48"
}

variable "smarty_streets_auth_token" {
  description = "The smarty streets auth token, provided in environment variables, for the environment."
  default     = "8lJDQhnct64d7NYO98NX"
}

variable "env" {
  description = "Environment name that must be unique in the entire org"
  default     = "provata-stage"
}

variable "base_repo_uri" {
  description = "The base container repository URI, used by all services"
  default     = "514127878762.dkr.ecr.us-west-2.amazonaws.com"
}

variable "default_replica_count" {
  default = "1"
}

variable "reports_replica_count" {
  default = "0"
}

variable "ecs_min_size" {
  default = "4"
}

variable "elk_instance_type" {
  default = "m4.large.elasticsearch"
}

variable "elk_instance_count" {
  default = "1"
}

variable "default_cpu" {
  default = "512"
}

variable "eligibility_cpu" {
  default = "512"
}

# the cpu and mem values are heavily coorelated as they need to fit nicely
# into the cpu/mem deminsions of the EC2 instances our cluster is using
# 7683 is the effective memory count for an m5.large instance
variable "default_mem" {
  default = "1920"
}

variable "eligibility_mem" {
  default = "1920"
}

variable "core_env" {
  default = "core-np"
}

variable "region" {
  default = "us-east-2"
}

variable "domain_name" {
  default = "stage-staywell.com"
}

variable "subdomain_name" {
  default = "hthu"
}

variable "redis_port" {
  default = "6379"
}

variable "redshift_port" {
  default = "5439"
}

variable "auth_port" {
  default = "7506"
}

variable "billing_port" {
  default = "7504"
}

variable "challenge_port" {
  default = "7502"
}

variable "core_port" {
  default = "7500"
}

variable "core_timeout" {
  default = "10"
}

variable "eligibility_port" {
  default = "7501"
}

variable "email_port" {
  default = "7503"
}

variable "integration_port" {
  default = "7510"
}

variable "native_port" {
  default = "3001"
}

variable "progress_port" {
  default = "7507"
}

variable "supplies_port" {
  default = "7505"
}

variable "vitals_port" {
  default = "7509"
}

variable "data_science_port" {
  default = "7511"
}

variable "web_port" {
  default = "3000"
}

variable "pvr_port" {
  default = "7599"
}

variable "reports_port" {
  default = "7600"
}

variable "scaling_min" {
  default = "1"
}

variable "scaling_max" {
  default = "4"
}

variable "cpu_scaling_target" {
  default = "50"
}

variable "elk_port" {
  default = "80"
}

variable "eligibility_export_process_count" {
  default = "100"
}

variable "eligibility_import_notification_username" {
  default = "import-user"
}

variable "eligibility_import_byte_batch_size" {
  default = "500000"
}

variable "eligibility_import_data_staging_error_threshold" {
  default = "100"
}

variable "fond_saml_post_url" {
  default = "https://staging1.weperk.com/saml/acs.saml2"
}

variable "fond_saml_issuer_template" {
  default = "https://%s.not-prod.hthu.com"
}

variable "fond_saml_audience_base" {
  default = "https://staging1.weperk.com/saml/"
}

variable "fond_company_domain_template" {
  default = "%s.not-prod.hthu.com"
}

variable "fond_staywell_relative_logo_url" {
  default = "/changeme.png"
}

variable "fond_saml_post_url_sandbox" {
  default = "https://sandbox.weperk.com/saml/acs.saml2"
}

variable "fond_saml_issuer_template_sandbox" {
  default = ""
}

variable "fond_saml_audience_base_sandbox" {
  default = "https://sandbox.weperk.com/saml/"
}

variable "fond_company_domain_template_sandbox" {
  default = ""
}

variable "fond_staywell_relative_logo_url_sandbox" {
  default = "/changeme.png"
}

variable "fond_api_url" {
  default = "https://staging1-api.weperk.com/v1"
}

variable "fond_api_url_sandbox" {
  default = "https://sandbox-api.weperk.com/v1"
}

variable "saml_cert_filename" {
  default = "ProvataSamlTest.p12"
}

variable "saml_cert_alias" {
  default = "provatasamltest"
}

variable "quest_post_url" {
  default = "https://my.questforhealth.com/sso/SignIn"
}

variable "quest_issuer" {
  default = "https://%s.test.hthu.com"
}

variable "provata_saml_post_url" {
  default = ""
}

variable "provata_saml_issuer" {
  default = "https://osi.test.hthu.com"
}

variable "help_scout_base_mailbox_id" {
  default = "135782"
}

variable "help_scout_api_client_id" {
  default = "7464383944884a9481fc9170518fffd2"
}

variable "help_scout_api_client_secret" {
  default = "5f0a98fe6bab436fb2c0a8584bccdd62"
}

variable "human_api_app_key" {
  default = "84d0a802db617a2e94b72d4fd662eff778ff71b2"
}

variable "human_api_client_id" {
  default = "9ac62dfbc218687579ef2e37e9ea672a5516863d"
}

variable "human_api_secret_key" {
  default = "12cde533cd4953bdc604f570a88a0ccc6c7aaede"
}

variable "redis_replicas_per_node_group" {
  default = "2"
}

variable "redis_num_node_groups" {
  default = "2"
}

variable "eligibility_file_upload_max_size_limit_kb" {
  default = "500000"
}