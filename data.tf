data "aws_route53_zone" "public" {
  name         = "${var.domain_name}."
  private_zone = false
}

data "aws_vpc" "core" {
  tags = {
    Environment = "${var.core_env}"
  }
}

data "aws_subnet_ids" "public" {
  vpc_id = "${data.aws_vpc.core.id}"

  tags = {
    Type = "Public"
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = "${data.aws_vpc.core.id}"

  tags = {
    Type = "Private"
  }
}

data "aws_ami" "this" {
  filter {
    name   = "name"
    values = ["*amazon-ecs-optimized*"]
  }

  most_recent = true
  owners      = ["amazon"]
}

data "aws_elb_service_account" "this" {}

data "aws_iam_role" "this" {
  name = "AWSServiceRoleForApplicationAutoScaling_ECSService"
}

data "aws_caller_identity" "this" {}
