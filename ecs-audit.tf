/*====
DynamoDB
======*/
resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "${var.env}-core-audit"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "auditRecord"
  range_key      = "dateTime"
  tags           = "${var.tags}"

  attribute = [
    {
      name = "auditRecord"
      type = "S"
    },
    {
      name = "dateTime"
      type = "S"
    },
  ]

  server_side_encryption {
    enabled = true
  }
}
