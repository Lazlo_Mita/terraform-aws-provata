resource "aws_ecs_task_definition" "auth" {
  family                   = "${var.env}-hthu-auth"
  network_mode             = "bridge"
  requires_compatibilities = ["EC2"]
  task_role_arn            = "${aws_iam_role.this.arn}"
  cpu                      = "${var.default_cpu}"
  memory                   = "${var.default_mem}"

  container_definitions = "${data.template_file.hthu_auth_template.rendered}"

}

/* The container template file */
data "template_file" "hthu_auth_template" {
  template = "${file("${path.module}/container_definitions/hthu-standard.json.tpl")}"

  vars {
    name      = "hthu-auth"
    image     = "${var.base_repo_uri}/hthu-auth:${var.versions["hthu-auth"]}"
    port      = "${var.auth_port}"
    region    = "${var.region}"
    log_path  = "hthu-auth"
    log_group = "${aws_cloudwatch_log_group.auth.name}"

    env_vars = "${
      jsonencode(
        concat(
          local.shared_env_vars,
          list(
            map("name", "SNS_TOPIC_ARN", "value", "${aws_sns_topic.hthu.arn}"),
            map("name", "DB_CONNECTIONS_MIN", "value", "4"),
            map("name", "DB_CONNECTIONS_INIT", "value", "4"),
            map("name", "JWT_TOKEN_SECRET", "value", "cpk5X#5?^gTDaU&C5eq-k0S|K3c-AvpmxYQPk24_1d7?f3x?rJ"),
            map("name", "FOND_SAML_POST_URL", "value", "${var.fond_saml_post_url}"),
            map("name", "FOND_SAML_ISSUER_TEMPLATE", "value", "${var.fond_saml_issuer_template}"),
            map("name", "FOND_SAML_AUDIENCE_BASE", "value", "${var.fond_saml_audience_base}"),
            map("name", "FOND_COMPANY_DOMAIN_TEMPLATE", "value", "${var.fond_company_domain_template}"),
            map("name", "FOND_STAYWELL_RELATIVE_LOGO_URL", "value", "${var.fond_staywell_relative_logo_url}"),
            map("name", "FOND_SAML_POST_URL_SANDBOX", "value", "${var.fond_saml_post_url_sandbox}"),
            map("name", "FOND_SAML_ISSUER_TEMPLATE_SANDBOX", "value", "${var.fond_saml_issuer_template_sandbox}"),
            map("name", "FOND_SAML_AUDIENCE_BASE_SANDBOX", "value", "${var.fond_saml_audience_base_sandbox}"),
            map("name", "FOND_COMPANY_DOMAIN_TEMPLATE_SANDBOX", "value", "${var.fond_company_domain_template_sandbox}"),
            map("name", "FOND_STAYWELL_RELATIVE_LOGO_URL_SANDBOX", "value", "${var.fond_staywell_relative_logo_url_sandbox}"),
            map("name", "QUEST_POST_URL", "value", "${var.quest_post_url}"),
            map("name", "QUEST_ISSUER", "value", "${var.quest_issuer}"),
            map("name", "SAML_CERT_FILENAME", "value", "${var.saml_cert_filename}"),
            map("name", "SAML_CERT_ALIAS", "value", "${var.saml_cert_alias}"),
            map("name", "PROVATA_SAML_POST_URL", "value", "${var.provata_saml_post_url}"),
            map("name", "PROVATA_SAML_ISSUER", "value", "${var.provata_saml_issuer}"),
            map("name", "DEFAULT_PASSWORD", "value", "a9c1392e31bc7d1eee4105ce14ba0273939d2406593f9c05e5f29c92ebddc55a"),
            map("name", "DEFAULT_SALT", "value", "Vjn5kgdSGsLYVxO6")
          )
        )
      )
    }"
  }
}

resource "aws_ecs_service" "auth" {
  name            = "hthu-auth"
  cluster         = "${module.ecs.cluster_id}"
  task_definition = "${aws_ecs_task_definition.auth.arn}"
  desired_count   = "${var.default_replica_count}"
  launch_type     = "EC2"

  load_balancer {
    target_group_arn = "${aws_lb_target_group.auth.id}"
    container_name   = "hthu-auth"
    container_port   = "${var.auth_port}"
  }

  lifecycle {
    ignore_changes = ["desired_count"]
  }
}

#################################################
#	Auto-Scaling
#################################################

resource "aws_appautoscaling_target" "auth_scaling_target" {
  max_capacity       = "${var.scaling_max}"
  min_capacity       = "${var.scaling_min}"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.auth.name}"
  role_arn           = "${data.aws_iam_role.this.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

/* resource "aws_appautoscaling_policy" "auth_scaling_policy" {
  name               = "${module.ecs.cluster_id}-${aws_ecs_service.auth.name}-scale-down"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.auth.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  target_tracking_scaling_policy_configuration {
    target_value = "${var.cpu_scaling_target}"

    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }

  depends_on = ["aws_appautoscaling_target.auth_scaling_target"]
} */

#################################################
#	Cloudwatch Log Group
#################################################

resource "aws_cloudwatch_log_group" "auth" {
  name = "${var.env}-auth"
  tags = "${var.tags}"
}

#################################################
#	Load Balancing
#################################################

resource "aws_lb_target_group" "auth" {
  name        = "${var.env}-auth"
  port        = "${var.auth_port}"
  protocol    = "HTTP"
  vpc_id      = "${data.aws_vpc.core.id}"
  target_type = "instance"

  health_check {
    path = "/admin/healthcheck"
  }
}

resource "aws_lb_listener" "internal_auth" {
  load_balancer_arn = "${aws_lb.internal.arn}"
  port              = "${var.auth_port}"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.auth.id}"
    type             = "forward"
  }
}

#################################################
#	DNS
#################################################

resource "aws_route53_record" "private_auth" {
  name    = "auth.hthu"
  type    = "A"
  zone_id = "${aws_route53_zone.private.zone_id}"

  alias {
    name                   = "${aws_lb.internal.dns_name}"
    zone_id                = "${aws_lb.internal.zone_id}"
    evaluate_target_health = false
  }
}
