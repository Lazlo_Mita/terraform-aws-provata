resource "aws_ecs_task_definition" "challenge" {
  family                   = "${var.env}-hthu-challenge"
  network_mode             = "bridge"
  requires_compatibilities = ["EC2"]
  task_role_arn            = "${aws_iam_role.this.arn}"
  cpu                      = "${var.default_cpu}"
  memory                   = "${var.default_mem}"

  container_definitions = "${data.template_file.hthu_challenge_template.rendered}"
}

/* The container template file */
data "template_file" "hthu_challenge_template" {
  template = "${file("${path.module}/container_definitions/hthu-standard.json.tpl")}"

  vars {
    name      = "hthu-challenge"
    image     = "${var.base_repo_uri}/hthu-challenge:${var.versions["hthu-challenge"]}"
    port      = "${var.challenge_port}"
    region    = "${var.region}"
    log_path  = "hthu-challenge"
    log_group = "${aws_cloudwatch_log_group.challenge.name}"

    env_vars = "${
      jsonencode(
        concat(
          local.shared_env_vars,
          list(
            map("name", "SNS_TOPIC_ARN", "value", "${aws_sns_topic.hthu.arn}"),
            map("name", "SQS_QUEUE_URL", "value", "${aws_sqs_queue.challenge.id}"),
            map("name", "DB_CONNECTIONS_MIN", "value", "4"),
            map("name", "DB_CONNECTIONS_INIT", "value", "4")
          )
        )
      )
    }"
  }
}

resource "aws_ecs_service" "challenge" {
  name            = "hthu-challenge"
  cluster         = "${module.ecs.cluster_id}"
  task_definition = "${aws_ecs_task_definition.challenge.arn}"
  desired_count   = "${var.default_replica_count}"
  launch_type     = "EC2"

  load_balancer {
    target_group_arn = "${aws_lb_target_group.challenge.id}"
    container_name   = "hthu-challenge"
    container_port   = "${var.challenge_port}"
  }

  lifecycle {
    ignore_changes = ["desired_count"]
  }
}

#################################################
#	Auto-Scaling
#################################################

resource "aws_appautoscaling_target" "challenge_scaling_target" {
  max_capacity       = "${var.scaling_max}"
  min_capacity       = "${var.scaling_min}"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.challenge.name}"
  role_arn           = "${data.aws_iam_role.this.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

/* resource "aws_appautoscaling_policy" "challenge_scaling_policy" {
  name               = "${module.ecs.cluster_id}-${aws_ecs_service.challenge.name}-scale-down"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.challenge.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  target_tracking_scaling_policy_configuration {
    target_value = "${var.cpu_scaling_target}"

    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }

  depends_on = ["aws_appautoscaling_target.challenge_scaling_target"]
} */

#################################################
#	Cloudwatch Log Group
#################################################

resource "aws_cloudwatch_log_group" "challenge" {
  name = "${var.env}-challenge"
  tags = "${var.tags}"
}

#################################################
#	Load Balancing
#################################################

resource "aws_lb_target_group" "challenge" {
  name        = "${var.env}-challenge"
  port        = "${var.challenge_port}"
  protocol    = "HTTP"
  vpc_id      = "${data.aws_vpc.core.id}"
  target_type = "instance"

  health_check {
    path = "/admin/healthcheck"
  }
}

resource "aws_lb_listener" "internal_challenge" {
  load_balancer_arn = "${aws_lb.internal.arn}"
  port              = "${var.challenge_port}"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.challenge.id}"
    type             = "forward"
  }
}

#################################################
#	DNS
#################################################

resource "aws_route53_record" "private_challenge" {
  name    = "challenge.hthu"
  type    = "A"
  zone_id = "${aws_route53_zone.private.zone_id}"

  alias {
    name                   = "${aws_lb.internal.dns_name}"
    zone_id                = "${aws_lb.internal.zone_id}"
    evaluate_target_health = false
  }
}

#################################################
#	Messaging Bus
#################################################

resource "aws_sqs_queue" "challenge" {
  name = "${var.env}-challenge"
  tags = "${var.tags}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [{
    "Effect": "Allow",
    "Principal": {
      "AWS": "*"
    },
    "Action": "SQS:SendMessage",
    "Resource": "arn:aws:sqs:${var.region}:${data.aws_caller_identity.this.account_id}:${var.env}-challenge",
    "Condition": {
      "ArnEquals": {
        "aws:SourceArn": "${aws_sns_topic.hthu.arn}"
      }
    }
  }]
}
POLICY
}
