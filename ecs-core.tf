resource "aws_ecs_task_definition" "core" {
  family                   = "${var.env}-hthu-core"
  network_mode             = "bridge"
  requires_compatibilities = ["EC2"]
  task_role_arn            = "${aws_iam_role.this.arn}"
  cpu                      = "${var.default_cpu}"
  memory                   = "${var.default_mem}"

  container_definitions = "${data.template_file.hthu_core_template.rendered}"
}

/* The container template file */
data "template_file" "hthu_core_template" {
  template = "${file("${path.module}/container_definitions/hthu-standard.json.tpl")}"

  vars {
    name      = "hthu-core"
    image     = "${var.base_repo_uri}/hthu-core:${var.versions["hthu-core"]}"
    port      = "${var.core_port}"
    region    = "${var.region}"
    log_path  = "hthu-core"
    log_group = "${aws_cloudwatch_log_group.core.name}"

    env_vars = "${
      jsonencode(
        concat(
          local.shared_env_vars,
          list(
            map("name", "SNS_TOPIC_ARN", "value", "${aws_sns_topic.hthu.arn}"),
            map("name", "SQS_QUEUE_URL", "value", "${aws_sqs_queue.core.id}"),
            map("name", "ELASTICSEARCH_HOST_NAME", "value", "${aws_elasticsearch_domain.this.endpoint}"),
            map("name", "ELASTICSEARCH_PORT", "value", "${var.elk_port}"),
            map("name", "HELP_SCOUT_BASE_MAILBOX_ID", "value", "${var.help_scout_base_mailbox_id}"),
            map("name", "HELP_SCOUT_API_BASE_URL", "value", "https://api.helpscout.net/v2"),
            map("name", "HELP_SCOUT_API_TOKEN_URL", "value", "https://api.helpscout.net/v2/oauth2/token"),
            map("name", "HELP_SCOUT_API_CLIENT_ID", "value", "${var.help_scout_api_client_id}"),
            map("name", "HELP_SCOUT_API_CLIENT_SECRET", "value", "${var.help_scout_api_client_secret}"),
            map("name", "HELP_SCOUT_API_TOKEN", "value", "21777d4d600e00960fcc0fe89abed20d49ee4347"),
            map("name", "HELP_SCOUT_WEB_HOOK_SECRET_KEY", "value", "5ymO7byoqM1RyRtheAfm"),
            map("name", "AUTO_TEAM_EXPIRE_DAYS", "value", "14"),
            map("name", "DB_CONNECTIONS_MIN", "value", "4"),
            map("name", "DB_CONNECTIONS_INIT", "value", "4"),
            map("name", "CACHE_TRANSLATIONS_IN_REDIS", "value", "true")
          )
        )
      )
    }"
  }
}

resource "aws_ecs_service" "core" {
  name                              = "hthu-core"
  cluster                           = "${module.ecs.cluster_id}"
  task_definition                   = "${aws_ecs_task_definition.core.arn}"
  desired_count                     = "${var.default_replica_count}"
  launch_type                       = "EC2"
  health_check_grace_period_seconds = "180"

  load_balancer {
    target_group_arn = "${aws_lb_target_group.core.id}"
    container_name   = "hthu-core"
    container_port   = "${var.core_port}"
  }

  lifecycle {
    ignore_changes = ["desired_count"]
  }
}

#################################################
#	Auto-Scaling
#################################################

resource "aws_appautoscaling_target" "core_scaling_target" {
  max_capacity       = "${var.scaling_max}"
  min_capacity       = "${var.scaling_min}"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.core.name}"
  role_arn           = "${data.aws_iam_role.this.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

/* resource "aws_appautoscaling_policy" "core_scaling_policy" {
  name               = "${module.ecs.cluster_id}-${aws_ecs_service.core.name}-scale-down"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.core.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  target_tracking_scaling_policy_configuration {
    target_value = "${var.cpu_scaling_target}"

    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }

  depends_on = ["aws_appautoscaling_target.core_scaling_target"]
} */

#################################################
#	Cloudwatch Log Group
#################################################

resource "aws_cloudwatch_log_group" "core" {
  name = "${var.env}-core"
  tags = "${var.tags}"
}

#################################################
#	Load Balancing
#################################################

resource "aws_lb_target_group" "core" {
  name        = "${var.env}-core"
  port        = "${var.core_port}"
  protocol    = "HTTP"
  vpc_id      = "${data.aws_vpc.core.id}"
  target_type = "instance"

  health_check {
    path    = "/admin/healthcheck"
    timeout = "${var.core_timeout}"
  }
}

resource "aws_lb_listener" "internal_core" {
  load_balancer_arn = "${aws_lb.internal.arn}"
  port              = "${var.core_port}"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.core.id}"
    type             = "forward"
  }
}

#################################################
#	DNS
#################################################

resource "aws_route53_record" "private_core" {
  name    = "core.hthu"
  type    = "A"
  zone_id = "${aws_route53_zone.private.zone_id}"

  alias {
    name                   = "${aws_lb.internal.dns_name}"
    zone_id                = "${aws_lb.internal.zone_id}"
    evaluate_target_health = false
  }
}

#################################################
#	Messaging Bus
#################################################

resource "aws_sqs_queue" "core" {
  name = "${var.env}-core"
  tags = "${var.tags}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [{
    "Effect": "Allow",
    "Principal": {
      "AWS": "*"
    },
    "Action": "SQS:SendMessage",
    "Resource": "arn:aws:sqs:${var.region}:${data.aws_caller_identity.this.account_id}:${var.env}-core",
    "Condition": {
      "ArnEquals": {
        "aws:SourceArn": "${aws_sns_topic.hthu.arn}"
      }
    }
  }]
}
POLICY
}
