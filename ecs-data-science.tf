resource "aws_ecs_task_definition" "data_science" {
  family                   = "${var.env}-hthu-data-science"
  network_mode             = "bridge"
  requires_compatibilities = ["EC2"]
  task_role_arn            = "${aws_iam_role.this.arn}"
  cpu                      = "${var.default_cpu}"
  memory                   = "${var.default_mem}"

  container_definitions = "${data.template_file.hthu_data_science_template.rendered}"

}

/* The container template file */
data "template_file" "hthu_data_science_template" {
  template = "${file("${path.module}/container_definitions/hthu-standard.json.tpl")}"

  vars {
    name      = "hthu-data-science"
    image     = "${var.base_repo_uri}/hthu-data-science:${var.versions["hthu-data-science"]}"
    port      = "${var.data_science_port}"
    region    = "${var.region}"
    log_path  = "hthu-data-science"
    log_group = "${aws_cloudwatch_log_group.data_science.name}"

    env_vars = "${
      jsonencode(
        concat(
          local.shared_env_vars,
          list(
            map("name", "SERVICE_CREDENTIAL", "value", "${var.service_credentials}"),
            map("name", "SNS_TOPIC_ARN", "value", "${aws_sns_topic.hthu.arn}"),
            map("name", "SQS_QUEUE_URL", "value", "${aws_sqs_queue.data_science.id}"),
            map("name", "SQS_QUEUE_FIFO_URL", "value", "${aws_sqs_queue.data_science_fifo.id}"),
            map("name", "PUBLIC_HTHU_WILDCARD", "value", "${aws_route53_record.public_hthu_wildcard.name}"),
            map("name", "DB_CONNECTIONS_MIN", "value", "4"),
            map("name", "DB_CONNECTIONS_INIT", "value", "4")
          )
        )
      )
    }"
  }
}

resource "aws_ecs_service" "data_science" {
  name            = "hthu-data-science"
  cluster         = "${module.ecs.cluster_id}"
  task_definition = "${aws_ecs_task_definition.data_science.arn}"
  desired_count   = "${var.default_replica_count}"
  launch_type     = "EC2"

  load_balancer {
    target_group_arn = "${aws_lb_target_group.data_science.id}"
    container_name   = "hthu-data-science"
    container_port   = "${var.data_science_port}"
  }

  lifecycle {
    ignore_changes = ["desired_count"]
  }
}

#################################################
#	Auto-Scaling
#################################################

resource "aws_appautoscaling_target" "data_science_scaling_target" {
  max_capacity       = "${var.scaling_max}"
  min_capacity       = "${var.scaling_min}"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.data_science.name}"
  role_arn           = "${data.aws_iam_role.this.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

/* resource "aws_appautoscaling_policy" "data_science_scaling_policy" {
  name               = "${module.ecs.cluster_id}-${aws_ecs_service.data_science.name}-scale-down"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.data_science.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  target_tracking_scaling_policy_configuration {
    target_value = "${var.cpu_scaling_target}"

    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }

  depends_on = ["aws_appautoscaling_target.data_science_scaling_target"]
} */

#################################################
#	Cloudwatch Log Group
#################################################

resource "aws_cloudwatch_log_group" "data_science" {
  name = "${var.env}-data-science"
  tags = "${var.tags}"
}

#################################################
#	Load Balancing
#################################################

resource "aws_lb_target_group" "data_science" {
  name        = "${var.env}-data-science"
  port        = "${var.data_science_port}"
  protocol    = "HTTP"
  vpc_id      = "${data.aws_vpc.core.id}"
  target_type = "instance"

  health_check {
    path = "/healthcheck"
  }
}

resource "aws_lb_listener" "internal_data_science" {
  load_balancer_arn = "${aws_lb.internal.arn}"
  port              = "${var.data_science_port}"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.data_science.id}"
    type             = "forward"
  }
}

#################################################
#	DNS
#################################################

resource "aws_route53_record" "private_data_science" {
  name    = "data.science.hthu"
  type    = "A"
  zone_id = "${aws_route53_zone.private.zone_id}"

  alias {
    name                   = "${aws_lb.internal.dns_name}"
    zone_id                = "${aws_lb.internal.zone_id}"
    evaluate_target_health = false
  }
}

#################################################
#	Messaging Bus
#################################################

resource "aws_sqs_queue" "data_science" {
  name = "${var.env}-data-science"
  tags = "${var.tags}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "SQS:SendMessage",
      "Resource": "arn:aws:sqs:${var.region}:${data.aws_caller_identity.this.account_id}:${var.env}-data-science",
      "Condition": {
        "StringEquals": {
          "aws:SourceArn": "${aws_sns_topic.hthu.arn}"
        }
      }
    }
  ]
}
POLICY
}

resource "aws_sqs_queue" "data_science_fifo" {
  name                        = "${var.env}-data-science.fifo"
  tags                        = "${var.tags}"
  fifo_queue                  = true
  content_based_deduplication = true

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "SQS:*",
      "Resource": "arn:aws:sqs:${var.region}:${data.aws_caller_identity.this.account_id}:${var.env}-data-science.fifo",
      "Condition": {
        "StringEquals": {
          "aws:SourceArn": "${aws_iam_role.this.arn}"
        }
      }
    }
  ]
}
POLICY
}
