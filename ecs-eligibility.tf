resource "aws_ecs_task_definition" "eligibility" {
  family                   = "${var.env}-hthu-eligibility"
  network_mode             = "bridge"
  requires_compatibilities = ["EC2"]
  task_role_arn            = "${aws_iam_role.this.arn}"
  cpu                      = "${var.eligibility_cpu}"
  memory                   = "${var.eligibility_mem}"

  container_definitions = "${data.template_file.hthu_eligibility_template.rendered}"
}

/* The container template file */
data "template_file" "hthu_eligibility_template" {
  template = "${file("${path.module}/container_definitions/hthu-standard.json.tpl")}"

  vars {
    name      = "hthu-eligibility"
    image     = "${var.base_repo_uri}/hthu-eligibility:${var.versions["hthu-eligibility"]}"
    port      = "${var.eligibility_port}"
    region    = "${var.region}"
    log_path  = "hthu-eligibility"
    log_group = "${aws_cloudwatch_log_group.eligibility.name}"

    env_vars = "${
      jsonencode(
        concat(
          local.shared_env_vars,
          list(
            map("name", "SNS_TOPIC_ARN", "value", "${aws_sns_topic.hthu.arn}"),
            map("name", "SQS_QUEUE_URL", "value", "${aws_sqs_queue.eligibility.id}"),
            map("name", "SFTP_SECRET_ARN", "value", "${aws_secretsmanager_secret.this.arn}"),
            map("name", "SFTP_USER_KEY", "value", "${var.sftp_user_key}"),
            map("name", "SFTP_PASS_KEY", "value", "${var.sftp_pass_key}"),
            map("name", "DB_CONNECTIONS_MIN", "value", "4"),
            map("name", "DB_CONNECTIONS_INIT", "value", "4"),
            map("name", "ELIGIBILITY_EXPORT_URL", "value", "eft.staywell.com"),
            map("name", "ELIGIBILITY_EXPORT_PROCESS_COUNT", "value", "${var.eligibility_export_process_count}"),
            map("name", "ELIGIBILITY_IMPORT_NOTIFICATION_USERNAME", "value", "${var.eligibility_import_notification_username}"),
            map("name", "ELIGIBILITY_IMPORT_BYTE_BATCH_SIZE", "value", "${var.eligibility_import_byte_batch_size}"),
            map("name", "ELIGIBILITY_IMPORT_DATA_STAGING_ERROR_THRESHOLD", "value", "${var.eligibility_import_data_staging_error_threshold}")
          )
        )
      )
    }"
  }
}

resource "aws_ecs_service" "eligibility" {
  name            = "hthu-eligibility"
  cluster         = "${module.ecs.cluster_id}"
  task_definition = "${aws_ecs_task_definition.eligibility.arn}"
  desired_count   = "${var.default_replica_count}"
  launch_type     = "EC2"

  load_balancer {
    target_group_arn = "${aws_lb_target_group.eligibility.id}"
    container_name   = "hthu-eligibility"
    container_port   = "${var.eligibility_port}"
  }

  lifecycle {
    ignore_changes = ["desired_count"]
  }
}

#################################################
#	Auto-Scaling
#################################################

resource "aws_appautoscaling_target" "eligibility_scaling_target" {
  max_capacity       = "${var.scaling_max}"
  min_capacity       = "${var.scaling_min}"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.eligibility.name}"
  role_arn           = "${data.aws_iam_role.this.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

/* resource "aws_appautoscaling_policy" "eligibility_scaling_policy" {
  name               = "${module.ecs.cluster_id}-${aws_ecs_service.eligibility.name}-scale-down"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.eligibility.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  target_tracking_scaling_policy_configuration {
    target_value = "${var.cpu_scaling_target}"

    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }

  depends_on = ["aws_appautoscaling_target.eligibility_scaling_target"]
} */

#################################################
#	Cloudwatch Log Group
#################################################

resource "aws_cloudwatch_log_group" "eligibility" {
  name = "${var.env}-eligibility"
  tags = "${var.tags}"
}

#################################################
#	Load Balancing
#################################################

resource "aws_lb_target_group" "eligibility" {
  name        = "${var.env}-eligibility"
  port        = "${var.eligibility_port}"
  protocol    = "HTTP"
  vpc_id      = "${data.aws_vpc.core.id}"
  target_type = "instance"

  health_check {
    path = "/admin/healthcheck"
  }
}

resource "aws_lb_listener" "internal_eligibility" {
  load_balancer_arn = "${aws_lb.internal.arn}"
  port              = "${var.eligibility_port}"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.eligibility.id}"
    type             = "forward"
  }
}

#################################################
#	DNS
#################################################

resource "aws_route53_record" "private_eligibility" {
  name    = "eligibility.hthu"
  type    = "A"
  zone_id = "${aws_route53_zone.private.zone_id}"

  alias {
    name                   = "${aws_lb.internal.dns_name}"
    zone_id                = "${aws_lb.internal.zone_id}"
    evaluate_target_health = false
  }
}

#################################################
#	Messaging Bus
#################################################

resource "aws_sqs_queue" "eligibility" {
  name = "${var.env}-eligibility"
  tags = "${var.tags}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [{
    "Effect": "Allow",
    "Principal": {
      "AWS": "*"
    },
    "Action": "SQS:SendMessage",
    "Resource": "arn:aws:sqs:${var.region}:${data.aws_caller_identity.this.account_id}:${var.env}-eligibility",
    "Condition": {
      "ArnEquals": {
        "aws:SourceArn": "${aws_sns_topic.hthu.arn}"
      }
    }
  }]
}
POLICY
}
