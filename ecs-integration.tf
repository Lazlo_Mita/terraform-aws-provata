resource "aws_ecs_task_definition" "integration" {
  family                   = "${var.env}-hthu-integration"
  network_mode             = "bridge"
  requires_compatibilities = ["EC2"]
  task_role_arn            = "${aws_iam_role.this.arn}"
  cpu                      = "${var.default_cpu}"
  memory                   = "${var.default_mem}"

  container_definitions = "${data.template_file.hthu_integration_template.rendered}"
}

/* The container template file */
data "template_file" "hthu_integration_template" {
  template = "${file("${path.module}/container_definitions/hthu-standard.json.tpl")}"

  vars {
    name      = "hthu-integration"
    image     = "${var.base_repo_uri}/hthu-integration:${var.versions["hthu-integration"]}"
    port      = "${var.integration_port}"
    region    = "${var.region}"
    log_path  = "hthu-integration"
    log_group = "${aws_cloudwatch_log_group.integration.name}"

    env_vars = "${
      jsonencode(
        concat(
          local.shared_env_vars,
          list(
            map("name", "SNS_TOPIC_ARN", "value", "${aws_sns_topic.hthu.arn}"),
            map("name", "SNS_INTEGRATION_TOPIC_ARN", "value", "${aws_sns_topic.integration.arn}"),
            map("name", "SQS_QUEUE_URL", "value", "${aws_sqs_queue.integration.id}"),
            map("name", "HUMAN_APP_KEY", "value", "${var.human_api_app_key}"),
            map("name", "HUMAN_SECRET_KEY", "value", "${var.human_api_secret_key}"),
            map("name", "HUMAN_CLIENT_ID", "value", "${var.human_api_client_id}"),
            map("name", "DB_CONNECTIONS_MIN", "value", "4"),
            map("name", "DB_CONNECTIONS_INIT", "value", "4")
          )
        )
      )
    }"
  }
}

resource "aws_ecs_service" "integration" {
  name            = "hthu-integration"
  cluster         = "${module.ecs.cluster_id}"
  task_definition = "${aws_ecs_task_definition.integration.arn}"
  desired_count   = "${var.default_replica_count}"
  launch_type     = "EC2"

  load_balancer {
    target_group_arn = "${aws_lb_target_group.integration.id}"
    container_name   = "hthu-integration"
    container_port   = "${var.integration_port}"
  }

  lifecycle {
    ignore_changes = ["desired_count"]
  }
}

#################################################
#	Auto-Scaling
#################################################

resource "aws_appautoscaling_target" "integration_scaling_target" {
  max_capacity       = "${var.scaling_max}"
  min_capacity       = "${var.scaling_min}"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.integration.name}"
  role_arn           = "${data.aws_iam_role.this.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

/* resource "aws_appautoscaling_policy" "integration_scaling_policy" {
  name               = "${module.ecs.cluster_id}-${aws_ecs_service.integration.name}-scale-down"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.integration.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  target_tracking_scaling_policy_configuration {
    target_value = "${var.cpu_scaling_target}"

    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }

  depends_on = ["aws_appautoscaling_target.integration_scaling_target"]
} */

#################################################
#	Cloudwatch Log Group
#################################################

resource "aws_cloudwatch_log_group" "integration" {
  name = "${var.env}-integration"
  tags = "${var.tags}"
}

#################################################
#	Load Balancing
#################################################

resource "aws_lb_target_group" "integration" {
  name        = "${var.env}-integration"
  port        = "${var.integration_port}"
  protocol    = "HTTP"
  vpc_id      = "${data.aws_vpc.core.id}"
  target_type = "instance"

  health_check {
    path = "/admin/healthcheck"
  }
}

resource "aws_lb_listener" "internal_integration" {
  load_balancer_arn = "${aws_lb.internal.arn}"
  port              = "${var.integration_port}"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.integration.id}"
    type             = "forward"
  }
}

#################################################
#	DNS
#################################################

resource "aws_route53_record" "private_integration" {
  name    = "integration.hthu"
  type    = "A"
  zone_id = "${aws_route53_zone.private.zone_id}"

  alias {
    name                   = "${aws_lb.internal.dns_name}"
    zone_id                = "${aws_lb.internal.zone_id}"
    evaluate_target_health = false
  }
}

#################################################
#	Messaging Bus
#################################################

resource "aws_sqs_queue" "integration" {
  name = "${var.env}-integration"
  tags = "${var.tags}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [{
    "Effect": "Allow",
    "Principal": {
      "AWS": "*"
    },
    "Action": "SQS:SendMessage",
    "Resource": "arn:aws:sqs:${var.region}:${data.aws_caller_identity.this.account_id}:${var.env}-integration",
    "Condition": {
      "ArnEquals": {
        "aws:SourceArn": "${aws_sns_topic.hthu.arn}"
      }
    }
  }, {
    "Effect": "Allow",
    "Principal": {
      "AWS": "*"
    },
    "Action": "SQS:SendMessage",
    "Resource": "arn:aws:sqs:${var.region}:${data.aws_caller_identity.this.account_id}:${var.env}-integration",
    "Condition": {
      "ArnEquals": {
        "aws:SourceArn": "${aws_sns_topic.integration.arn}"
      }
    }
  }]
}
POLICY
}

resource "aws_sns_topic" "integration" {
  name                                  = "${var.env}-integration"
  application_success_feedback_role_arn = "${aws_iam_role.this.arn}"
  application_failure_feedback_role_arn = "${aws_iam_role.this.arn}"
  http_success_feedback_role_arn        = "${aws_iam_role.this.arn}"
  http_failure_feedback_role_arn        = "${aws_iam_role.this.arn}"
  lambda_success_feedback_role_arn      = "${aws_iam_role.this.arn}"
  lambda_failure_feedback_role_arn      = "${aws_iam_role.this.arn}"
  sqs_success_feedback_role_arn         = "${aws_iam_role.this.arn}"
  sqs_failure_feedback_role_arn         = "${aws_iam_role.this.arn}"
}
