resource "aws_ecs_task_definition" "native" {
  family                   = "${var.env}-hthu-native-service"
  network_mode             = "bridge"
  requires_compatibilities = ["EC2"]
  task_role_arn            = "${aws_iam_role.this.arn}"
  cpu                      = "${var.default_cpu}"
  memory                   = "${var.default_mem}"

  container_definitions = "${data.template_file.hthu_native_template.rendered}"
}

/* The container template file */
data "template_file" "hthu_native_template" {
  template = "${file("${path.module}/container_definitions/hthu-standard.json.tpl")}"

  vars {
    name      = "hthu-native-service"
    image     = "${var.base_repo_uri}/hthu-native-service:${var.versions["hthu-native-service"]}"
    port      = "${var.native_port}"
    region    = "${var.region}"
    log_path  = "hthu-native-service"
    log_group = "${aws_cloudwatch_log_group.native.name}"

    env_vars = "${
      jsonencode(
        concat(
          local.shared_env_vars,
          list(
            map("name", "ENABLE_NEWRELIC", "value", "true"),
            map("name", "PORT", "value", "${var.native_port}"),
            map("name", "SERVICE_CREDS", "value", "${var.service_credentials}"),
            map("name", "SMARTY_STREETS_AUTH_ID", "value", "${var.smarty_streets_auth_id}"),
            map("name", "SMARTY_STREETS_AUTH_TOKEN", "value", "${var.smarty_streets_auth_token}"),
            map("name", "NODE_ENV", "value", "${var.node_env}"),
            map("name", "SNS_TOPIC_ARN", "value", "${aws_sns_topic.hthu.arn}"),
            map("name", "SQS_QUEUE_URL", "value", "${aws_sqs_queue.native.id}")
          )
        )
      )
    }"
  }
}

resource "aws_ecs_service" "native" {
  name            = "hthu-native-service"
  cluster         = "${module.ecs.cluster_id}"
  task_definition = "${aws_ecs_task_definition.native.arn}"
  desired_count   = "${var.default_replica_count}"
  launch_type     = "EC2"

  load_balancer {
    target_group_arn = "${aws_lb_target_group.native.id}"
    container_name   = "hthu-native-service"
    container_port   = "${var.native_port}"
  }

  lifecycle {
    ignore_changes = ["desired_count"]
  }
}

#################################################
#	Auto-Scaling
#################################################

resource "aws_appautoscaling_target" "native_scaling_target" {
  max_capacity       = "${var.scaling_max}"
  min_capacity       = "${var.scaling_min}"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.native.name}"
  role_arn           = "${data.aws_iam_role.this.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

/* resource "aws_appautoscaling_policy" "native_scaling_policy" {
  name               = "${module.ecs.cluster_id}-${aws_ecs_service.native.name}-scale-down"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.native.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  target_tracking_scaling_policy_configuration {
    target_value = "${var.cpu_scaling_target}"

    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }

  depends_on = ["aws_appautoscaling_target.native_scaling_target"]
} */

#################################################
#	Cloudwatch Log Group
#################################################

resource "aws_cloudwatch_log_group" "native" {
  name = "${var.env}-native"
  tags = "${var.tags}"
}

#################################################
#	Load Balancing
#################################################

resource "aws_lb_target_group" "native" {
  name        = "${var.env}-native"
  port        = "${var.native_port}"
  protocol    = "HTTP"
  vpc_id      = "${data.aws_vpc.core.id}"
  target_type = "instance"

  health_check {
    path = "/health"
  }
}

resource "aws_lb_listener" "hthu_native" {
  load_balancer_arn = "${aws_lb.hthu.arn}"
  port              = "${var.native_port}"
  protocol          = "HTTPS"
  ssl_policy        = "${var.ssl_policy}"
  certificate_arn   = "${aws_acm_certificate.star_subdomain.arn}"

  default_action {
    target_group_arn = "${aws_lb_target_group.native.id}"
    type             = "forward"
  }
}

resource "aws_security_group_rule" "alb_ingress_native" {
  type              = "ingress"
  from_port         = "${var.native_port}"
  to_port           = "${var.native_port}"
  protocol          = "tcp"
  security_group_id = "${aws_security_group.alb.id}"
  cidr_blocks       = ["0.0.0.0/0"]
}

#################################################
#	Messaging Bus
#################################################

resource "aws_sqs_queue" "native" {
  name = "${var.env}-native"
  tags = "${var.tags}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [{
    "Effect": "Allow",
    "Principal": {
      "AWS": "*"
    },
    "Action": "SQS:SendMessage",
    "Resource": "arn:aws:sqs:${var.region}:${data.aws_caller_identity.this.account_id}:${var.env}-native",
    "Condition": {
      "ArnEquals": {
        "aws:SourceArn": "${aws_sns_topic.hthu.arn}"
      }
    }
  }]
}
POLICY
}
