resource "aws_ecs_task_definition" "pvr" {
  family                   = "${var.env}-pvr-service"
  network_mode             = "bridge"
  requires_compatibilities = ["EC2"]
  task_role_arn            = "${aws_iam_role.this.arn}"
  cpu                      = "${var.default_cpu}"
  memory                   = "${var.default_mem}"

  container_definitions = "${data.template_file.hthu_pvr_template.rendered}"
}

/* The container template file */
data "template_file" "hthu_pvr_template" {
  template = "${file("${path.module}/container_definitions/hthu-standard.json.tpl")}"

  vars {
    name      = "pvr-service"
    image     = "${var.base_repo_uri}/pvr-service:${var.versions["pvr-service"]}"
    port      = "${var.pvr_port}"
    region    = "${var.region}"
    log_path  = "pvr-service"
    log_group = "${aws_cloudwatch_log_group.pvr.name}"

    env_vars = "${
      jsonencode(
        concat(
          local.shared_env_vars,
          list(
            map("name", "SNS_TOPIC_ARN", "value", "${aws_sns_topic.pvr.arn}"),
            map("name", "SQS_QUEUE_URL", "value", "${aws_sqs_queue.pvr.id}"),
            map("name", "PVR_DB_HOST", "value", "${aws_db_instance.master.address}"),
            map("name", "DB_CONNECTIONS_MIN", "value", "4"),
            map("name", "DB_CONNECTIONS_INIT", "value", "4")
          )
        )
      )
    }"
  }
}

resource "aws_ecs_service" "pvr" {
  name            = "pvr-service"
  cluster         = "${module.ecs.cluster_id}"
  task_definition = "${aws_ecs_task_definition.pvr.arn}"
  desired_count   = "${var.default_replica_count}"
  launch_type     = "EC2"

  load_balancer {
    target_group_arn = "${aws_lb_target_group.pvr.id}"
    container_name   = "pvr-service"
    container_port   = "${var.pvr_port}"
  }

  lifecycle {
    ignore_changes = ["desired_count"]
  }
}

#################################################
#	Auto-Scaling
#################################################

resource "aws_appautoscaling_target" "pvr_scaling_target" {
  max_capacity       = "${var.scaling_max}"
  min_capacity       = "${var.scaling_min}"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.pvr.name}"
  role_arn           = "${data.aws_iam_role.this.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

/* resource "aws_appautoscaling_policy" "pvr_scaling_policy" {
  name               = "${module.ecs.cluster_id}-${aws_ecs_service.pvr.name}-scale-down"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.pvr.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  target_tracking_scaling_policy_configuration {
    target_value = "${var.cpu_scaling_target}"

    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }

  depends_on = ["aws_appautoscaling_target.pvr_scaling_target"]
} */

#################################################
#	Cloudwatch Log Group
#################################################

resource "aws_cloudwatch_log_group" "pvr" {
  name = "${var.env}-pvr"
  tags = "${var.tags}"
}

#################################################
#	Load Balancing
#################################################

resource "aws_lb" "pvr" {
  name               = "${var.env}-pvr"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.alb.id}"]
  subnets            = ["${data.aws_subnet_ids.public.ids}"]
  tags               = "${var.tags}"

  access_logs {
    bucket  = "${aws_s3_bucket.logs.bucket}"
    prefix  = "${var.env}-alb-pvr"
    enabled = true
  }
}

resource "aws_lb_target_group" "pvr" {
  name        = "${var.env}-pvr"
  port        = "${var.pvr_port}"
  protocol    = "HTTP"
  vpc_id      = "${data.aws_vpc.core.id}"
  target_type = "instance"

  health_check {
    path = "/admin/healthcheck?token=x73n9zzuKz6t1VWL4mGocGrliV7R1A2R"
  }
}

resource "aws_lb_listener" "pvr_80" {
  load_balancer_arn = "${aws_lb.pvr.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "pvr_443" {
  load_balancer_arn = "${aws_lb.pvr.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "${var.ssl_policy}"
  certificate_arn   = "${aws_acm_certificate.star_subdomain.arn}"

  default_action {
    target_group_arn = "${aws_lb_target_group.pvr.id}"
    type             = "forward"
  }
}

#################################################
#	DNS
#################################################

resource "aws_route53_record" "public_pvr" {
  name    = "pvr.${var.subdomain_name}.${var.domain_name}"
  type    = "CNAME"
  zone_id = "${data.aws_route53_zone.public.zone_id}"
  records = ["${aws_lb.pvr.dns_name}"]
  ttl     = 300
}

resource "aws_route53_record" "private_pvr" {
  name    = "service.pvr"
  type    = "A"
  zone_id = "${aws_route53_zone.private.zone_id}"

  alias {
    name                   = "${aws_lb.pvr.dns_name}"
    zone_id                = "${aws_lb.pvr.zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "private_redis_pvr" {
  name    = "redis.pvr"
  type    = "CNAME"
  zone_id = "${aws_route53_zone.private.zone_id}"
  records = ["${aws_elasticache_cluster.pvr.cache_nodes.0.address}"]
  ttl     = 300
}

resource "aws_route53_record" "private_db_pvr" {
  name    = "db.pvr"
  type    = "CNAME"
  zone_id = "${aws_route53_zone.private.zone_id}"
  records = ["${aws_db_instance.master.address}"]
  ttl     = 300
}

resource "aws_route53_record" "private_readonly_db_pvr" {
  name    = "readonly.db.pvr"
  type    = "CNAME"
  zone_id = "${aws_route53_zone.private.zone_id}"
  records = ["${aws_db_instance.replica.address}"]
  ttl     = 300
}

#################################################
#	Messaging Bus
#################################################

resource "aws_sqs_queue" "pvr" {
  name = "${var.env}-pvr"
  tags = "${var.tags}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [{
    "Effect": "Allow",
    "Principal": {
      "AWS": "*"
    },
    "Action": "SQS:SendMessage",
    "Resource": "arn:aws:sqs:${var.region}:${data.aws_caller_identity.this.account_id}:${var.env}-pvr",
    "Condition": {
      "ArnEquals": {
        "aws:SourceArn": "${aws_sns_topic.pvr.arn}"
      }
    }
  }]
}
POLICY
}

resource "aws_sns_topic" "pvr" {
  name                                  = "${var.env}-pvr"
  application_success_feedback_role_arn = "${aws_iam_role.this.arn}"
  application_failure_feedback_role_arn = "${aws_iam_role.this.arn}"
  http_success_feedback_role_arn        = "${aws_iam_role.this.arn}"
  http_failure_feedback_role_arn        = "${aws_iam_role.this.arn}"
  lambda_success_feedback_role_arn      = "${aws_iam_role.this.arn}"
  lambda_failure_feedback_role_arn      = "${aws_iam_role.this.arn}"
  sqs_success_feedback_role_arn         = "${aws_iam_role.this.arn}"
  sqs_failure_feedback_role_arn         = "${aws_iam_role.this.arn}"
}

resource "aws_sns_topic_subscription" "pvr_pvr" {
  topic_arn = "${aws_sns_topic.pvr.arn}"
  endpoint  = "${aws_sqs_queue.pvr.arn}"
  protocol  = "sqs"
}
