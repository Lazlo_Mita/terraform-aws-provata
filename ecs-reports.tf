resource "aws_ecs_task_definition" "reports" {
  family                   = "${var.env}-hthu-reports"
  network_mode             = "bridge"
  requires_compatibilities = ["EC2"]
  task_role_arn            = "${aws_iam_role.this.arn}"
  cpu                      = "${var.default_cpu}"
  memory                   = "${var.default_mem}"

  container_definitions = "${data.template_file.hthu_reports_template.rendered}"
}

/* The container template file */
data "template_file" "hthu_reports_template" {
  template = "${file("${path.module}/container_definitions/hthu-standard.json.tpl")}"

  vars {
    name      = "hthu-metabase"
    image     = "metabase/metabase:${var.versions["metabase"]}"
    port      = "${var.reports_port}"
    region    = "${var.region}"
    log_path  = "hthu-reports"
    log_group = "${aws_cloudwatch_log_group.reports.name}"

    env_vars = "${
      jsonencode(
        concat(
          local.shared_env_vars,
          list(
            map("name", "SNS_TOPIC_ARN", "value", "${aws_sns_topic.hthu.arn}"),
            map("name", "SQS_QUEUE_URL", "value", "${aws_sqs_queue.audit.id}"),
            map("name", "JAVA_TIMEZONE", "value", "${var.java_timezone}"),
            map("name", "MB_DB_DBNAME", "value", "metabase"),
            map("name", "MB_DB_HOST", "value", "db.hthu.provata.app"),
            map("name", "MB_DB_PASS", "value", "alR#ja3#j1asulkAxc!"),
            map("name", "MB_DB_PORT", "value", "3306"),
            map("name", "MB_DB_TYPE", "value", "mysql"),
            map("name", "MB_DB_USER", "value", "root")
          )
        )
      )
    }"
  }
}

resource "aws_ecs_service" "reports" {
  name            = "provata-reports"
  cluster         = "${module.ecs.cluster_id}"
  task_definition = "${aws_ecs_task_definition.reports.arn}"
  desired_count   = "${var.reports_replica_count}"
  launch_type     = "EC2"

  load_balancer {
    target_group_arn = "${aws_lb_target_group.reports.id}"
    container_name   = "hthu-metabase"
    container_port   = "${var.reports_port}"
  }

  lifecycle {
    ignore_changes = ["desired_count"]
  }
}

#################################################
#	Auto-Scaling
#################################################

resource "aws_appautoscaling_target" "reports_scaling_target" {
  max_capacity       = "${var.scaling_max}"
  min_capacity       = "${var.scaling_min}"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.reports.name}"
  role_arn           = "${data.aws_iam_role.this.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

/* resource "aws_appautoscaling_policy" "reports_scaling_policy" {
  name               = "${module.ecs.cluster_id}-${aws_ecs_service.reports.name}-scale-down"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.reports.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  target_tracking_scaling_policy_configuration {
    target_value = "${var.cpu_scaling_target}"

    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }

  depends_on = ["aws_appautoscaling_target.reports_scaling_target"]
} */

#################################################
#	Cloudwatch Log Group
#################################################

resource "aws_cloudwatch_log_group" "reports" {
  name = "${var.env}-reports"
  tags = "${var.tags}"
}

#################################################
#	Load Balancing
#################################################

resource "aws_lb" "reports" {
  name               = "${var.env}-reports"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.alb.id}"]
  subnets            = ["${data.aws_subnet_ids.public.ids}"]
  tags               = "${var.tags}"

  access_logs {
    bucket  = "${aws_s3_bucket.logs.bucket}"
    prefix  = "${var.env}-alb-reports"
    enabled = true
  }
}

resource "aws_lb_target_group" "reports" {
  name        = "${var.env}-reports"
  port        = "${var.reports_port}"
  protocol    = "HTTP"
  vpc_id      = "${data.aws_vpc.core.id}"
  target_type = "instance"
}

resource "aws_lb_listener" "reports_80" {
  load_balancer_arn = "${aws_lb.reports.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "reports_443" {
  load_balancer_arn = "${aws_lb.reports.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "${var.ssl_policy}"
  certificate_arn   = "${aws_acm_certificate.star_subdomain.arn}"

  default_action {
    target_group_arn = "${aws_lb_target_group.reports.id}"
    type             = "forward"
  }
}

#################################################
#	DNS
#################################################

#################################################
#	Messaging Bus
#################################################

resource "aws_sqs_queue" "audit" {
  name = "${var.env}-audit"
  tags = "${var.tags}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [{
    "Effect": "Allow",
    "Principal": {
      "AWS": "*"
    },
    "Action": "SQS:SendMessage",
    "Resource": "arn:aws:sqs:${var.region}:${data.aws_caller_identity.this.account_id}:${var.env}-audit",
    "Condition": {
      "ArnEquals": {
        "aws:SourceArn": "${aws_sns_topic.hthu.arn}"
      }
    }
  }, {
    "Effect": "Allow",
    "Principal": {
      "AWS": "*"
    },
    "Action": "SQS:SendMessage",
    "Resource": "arn:aws:sqs:${var.region}:${data.aws_caller_identity.this.account_id}:${var.env}-audit",
    "Condition": {
      "ArnEquals": {
        "aws:SourceArn": "${aws_sns_topic.integration.arn}"
      }
    }
  }, {
    "Effect": "Allow",
    "Principal": {
      "AWS": "*"
    },
    "Action": "SQS:SendMessage",
    "Resource": "arn:aws:sqs:${var.region}:${data.aws_caller_identity.this.account_id}:${var.env}-audit",
    "Condition": {
      "ArnEquals": {
        "aws:SourceArn": "${aws_sns_topic.email.arn}"
      }
    }
  }]
}
POLICY
}
