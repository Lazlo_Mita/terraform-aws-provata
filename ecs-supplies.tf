resource "aws_ecs_task_definition" "supplies" {
  family                   = "${var.env}-hthu-supplies"
  network_mode             = "bridge"
  requires_compatibilities = ["EC2"]
  task_role_arn            = "${aws_iam_role.this.arn}"
  cpu                      = "${var.default_cpu}"
  memory                   = "${var.default_mem}"

  container_definitions = "${data.template_file.hthu_supplies_template.rendered}"
}

/* The container template file */
data "template_file" "hthu_supplies_template" {
  template = "${file("${path.module}/container_definitions/hthu-standard.json.tpl")}"

  vars {
    name      = "hthu-supplies"
    image     = "${var.base_repo_uri}/hthu-supplies:${var.versions["hthu-supplies"]}"
    port      = "${var.supplies_port}"
    region    = "${var.region}"
    log_path  = "hthu-supplies"
    log_group = "${aws_cloudwatch_log_group.supplies.name}"

    env_vars = "${
      jsonencode(
        concat(
          local.shared_env_vars,
          list(
            map("name", "SNS_TOPIC_ARN", "value", "${aws_sns_topic.hthu.arn}"),
            map("name", "SQS_QUEUE_URL", "value", "${aws_sqs_queue.supplies.id}"),
            map("name", "SHIPPING_FTP_HOST", "value", "VALUE_NEEDED"),
            map("name", "SHIPPING_FTP_USERNAME", "value", "VALUE_NEEDED"),
            map("name", "SHIPPING_FTP_PASSWORD", "value", "VALUE_NEEDED"),
            map("name", "SHIPPING_FTP_PORT", "value", "21"),
            map("name", "SKIP_CUSTOMER_KEYS", "value", "osi"),
            map("name", "FTP_TOP_LEVEL_ONLY", "value", "VALUE_NEEDED"),
            map("name", "AWS_SHIPPING_FILE_BUCKET", "value", "VALUE_NEEDED"),
            map("name", "DB_CONNECTIONS_MIN", "value", "4"),
            map("name", "DB_CONNECTIONS_INIT", "value", "4")
          )
        )
      )
    }"
  }
}

resource "aws_ecs_service" "supplies" {
  name            = "hthu-supplies"
  cluster         = "${module.ecs.cluster_id}"
  task_definition = "${aws_ecs_task_definition.supplies.arn}"
  desired_count   = "${var.default_replica_count}"
  launch_type     = "EC2"

  load_balancer {
    target_group_arn = "${aws_lb_target_group.supplies.id}"
    container_name   = "hthu-supplies"
    container_port   = "${var.supplies_port}"
  }

  lifecycle {
    ignore_changes = ["desired_count"]
  }
}

#################################################
#	Auto-Scaling
#################################################

resource "aws_appautoscaling_target" "supplies_scaling_target" {
  max_capacity       = "${var.scaling_max}"
  min_capacity       = "${var.scaling_min}"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.supplies.name}"
  role_arn           = "${data.aws_iam_role.this.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

/* resource "aws_appautoscaling_policy" "supplies_scaling_policy" {
  name               = "${module.ecs.cluster_id}-${aws_ecs_service.supplies.name}-scale-down"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.supplies.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  target_tracking_scaling_policy_configuration {
    target_value = "${var.cpu_scaling_target}"

    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }

  depends_on = ["aws_appautoscaling_target.supplies_scaling_target"]
} */

#################################################
#	Cloudwatch Log Group
#################################################

resource "aws_cloudwatch_log_group" "supplies" {
  name = "${var.env}-supplies"
  tags = "${var.tags}"
}

#################################################
#	Load Balancing
#################################################

resource "aws_lb_target_group" "supplies" {
  name        = "${var.env}-supplies"
  port        = "${var.supplies_port}"
  protocol    = "HTTP"
  vpc_id      = "${data.aws_vpc.core.id}"
  target_type = "instance"

  health_check {
    path = "/admin/healthcheck"
  }
}

resource "aws_lb_listener" "internal_supplies" {
  load_balancer_arn = "${aws_lb.internal.arn}"
  port              = "${var.supplies_port}"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.supplies.id}"
    type             = "forward"
  }
}

#################################################
#	DNS
#################################################

resource "aws_route53_record" "private_supplies" {
  name    = "supplies.hthu"
  type    = "A"
  zone_id = "${aws_route53_zone.private.zone_id}"

  alias {
    name                   = "${aws_lb.internal.dns_name}"
    zone_id                = "${aws_lb.internal.zone_id}"
    evaluate_target_health = false
  }
}

#################################################
#	Messaging Bus
#################################################

resource "aws_sqs_queue" "supplies" {
  name = "${var.env}-supplies"
  tags = "${var.tags}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [{
    "Effect": "Allow",
    "Principal": {
      "AWS": "*"
    },
    "Action": "SQS:SendMessage",
    "Resource": "arn:aws:sqs:${var.region}:${data.aws_caller_identity.this.account_id}:${var.env}-supplies",
    "Condition": {
      "ArnEquals": {
        "aws:SourceArn": "${aws_sns_topic.hthu.arn}"
      }
    }
  }]
}
POLICY
}
