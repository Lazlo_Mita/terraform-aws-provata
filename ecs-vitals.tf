resource "aws_ecs_task_definition" "vitals" {
  family                   = "${var.env}-hthu-vitals"
  network_mode             = "bridge"
  requires_compatibilities = ["EC2"]
  task_role_arn            = "${aws_iam_role.this.arn}"
  cpu                      = "${var.default_cpu}"
  memory                   = "${var.default_mem}"

  container_definitions = "${data.template_file.hthu_vitals_template.rendered}"
}

/* The container template file */
data "template_file" "hthu_vitals_template" {
  template = "${file("${path.module}/container_definitions/hthu-standard.json.tpl")}"

  vars {
    name      = "hthu-vitals"
    image     = "${var.base_repo_uri}/hthu-vitals:${var.versions["hthu-vitals"]}"
    port      = "${var.vitals_port}"
    region    = "${var.region}"
    log_path  = "hthu-vitals"
    log_group = "${aws_cloudwatch_log_group.vitals.name}"

    env_vars = "${
      jsonencode(
        concat(
          local.shared_env_vars,
          list(
            map("name", "SNS_TOPIC_ARN", "value", "${aws_sns_topic.hthu.arn}"),
            map("name", "SQS_QUEUE_URL", "value", "${aws_sqs_queue.vitals.id}"),
            map("name", "BODYTRACE_INCOMING_AUTH_HEADER", "value", "Basic Ym9keVRyYWNlOjhZNEtWTUdES080T05ITUFOUjBNOFlVTUc3R0FRMQ=="),
            map("name", "HEALTHTAP_AUTHORIZATION", "value", "Basic XHx3oBSgBk3cuEYh7AGp9FUA6R61eziz"),
            map("name", "HEALTHTAP_PROVATA_APP_ID", "value", "1"),
            map("name", "HEALTHTAP_QUESTION_URL", "value", "http://localhost/healthtap/questions"),
            map("name", "HEALTHTAP_INCOMING_TOKEN", "value", "tD0785Y7z9f460df8R3Kc5vc898sw4wX"),
            map("name", "DB_CONNECTIONS_MIN", "value", "4"),
            map("name", "DB_CONNECTIONS_INIT", "value", "4")
          )
        )
      )
    }"
  }
}

resource "aws_ecs_service" "vitals" {
  name            = "hthu-vitals"
  cluster         = "${module.ecs.cluster_id}"
  task_definition = "${aws_ecs_task_definition.vitals.arn}"
  desired_count   = "${var.default_replica_count}"
  launch_type     = "EC2"

  load_balancer {
    target_group_arn = "${aws_lb_target_group.vitals.id}"
    container_name   = "hthu-vitals"
    container_port   = "${var.vitals_port}"
  }

  lifecycle {
    ignore_changes = ["desired_count"]
  }
}

#################################################
#	Auto-Scaling
#################################################

resource "aws_appautoscaling_target" "vitals_scaling_target" {
  max_capacity       = "${var.scaling_max}"
  min_capacity       = "${var.scaling_min}"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.vitals.name}"
  role_arn           = "${data.aws_iam_role.this.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

/* resource "aws_appautoscaling_policy" "vitals_scaling_policy" {
  name               = "${module.ecs.cluster_id}-${aws_ecs_service.vitals.name}-scale-down"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.vitals.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  target_tracking_scaling_policy_configuration {
    target_value = "${var.cpu_scaling_target}"

    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }

  depends_on = ["aws_appautoscaling_target.vitals_scaling_target"]
} */

#################################################
#	Cloudwatch Log Group
#################################################

resource "aws_cloudwatch_log_group" "vitals" {
  name = "${var.env}-vitals"
  tags = "${var.tags}"
}

#################################################
#	Load Balancing
#################################################

resource "aws_lb_target_group" "vitals" {
  name        = "${var.env}-vitals"
  port        = "${var.vitals_port}"
  protocol    = "HTTP"
  vpc_id      = "${data.aws_vpc.core.id}"
  target_type = "instance"

  health_check {
    path = "/admin/healthcheck"
  }
}

resource "aws_lb_listener" "internal_vitals" {
  load_balancer_arn = "${aws_lb.internal.arn}"
  port              = "${var.vitals_port}"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.vitals.id}"
    type             = "forward"
  }
}

#################################################
#	DNS
#################################################

resource "aws_route53_record" "private_vitals" {
  name    = "vitals.hthu"
  type    = "A"
  zone_id = "${aws_route53_zone.private.zone_id}"

  alias {
    name                   = "${aws_lb.internal.dns_name}"
    zone_id                = "${aws_lb.internal.zone_id}"
    evaluate_target_health = false
  }
}

#################################################
#	Messaging Bus
#################################################

resource "aws_sqs_queue" "vitals" {
  name = "${var.env}-vitals"
  tags = "${var.tags}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [{
    "Effect": "Allow",
    "Principal": {
      "AWS": "*"
    },
    "Action": "SQS:SendMessage",
    "Resource": "arn:aws:sqs:${var.region}:${data.aws_caller_identity.this.account_id}:${var.env}-vitals",
    "Condition": {
      "ArnEquals": {
        "aws:SourceArn": "${aws_sns_topic.hthu.arn}"
      }
    }
  }]
}
POLICY
}
