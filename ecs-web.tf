resource "aws_ecs_task_definition" "web" {
  family                   = "${var.env}-hthu-web"
  network_mode             = "bridge"
  requires_compatibilities = ["EC2"]
  task_role_arn            = "${aws_iam_role.this.arn}"
  cpu                      = "${var.default_cpu}"
  memory                   = "${var.default_mem}"

  container_definitions = "${data.template_file.hthu_web_template.rendered}"
}

/* The container template file */
data "template_file" "hthu_web_template" {
  template = "${file("${path.module}/container_definitions/hthu-standard.json.tpl")}"

  vars {
    name      = "hthu-web"
    image     = "${var.base_repo_uri}/hthu-web:${var.versions["hthu-web"]}"
    port      = "${var.web_port}"
    region    = "${var.region}"
    log_path  = "hthu-web"
    log_group = "${aws_cloudwatch_log_group.web.name}"

    env_vars = "${
      jsonencode(
        concat(
          local.shared_env_vars,
          list(
            map("name", "CDN_URL", "value", "https://${aws_cloudfront_distribution.this.domain_name}"),
            map("name", "DB_NAME", "value", "hthu-web"),
            map("name", "ELIGIBILITY_FILE_UPLOAD_MAX_SIZE_LIMIT_KB", "value", "${var.eligibility_file_upload_max_size_limit_kb}"),
            map("name", "ENABLE_NEWRELIC", "value", "true"),
            map("name", "ENABLE_SCANII", "value", "${var.enable_av}"),
            map("name", "FAKE_SCAN_CUSTOMER_KEYS", "value", "[]"),
            map("name", "FAKE_SCAN_FOR_ALL", "value", "${var.fake_av_scan}"),
            map("name", "HUMAN_API_TOKEN", "value", "9ac62dfbc218687579ef2e37e9ea672a5516863d"),
            map("name", "INTERCOM_APP_ID", "value", "k7n3a9xj"),
            map("name", "INTERCOM_SECRET", "value", "Ot-tZ3N6rJ6l5VH5s0gWXWHF7zLc6rOHyQD_PZHx"),
            map("name", "MAINTENANCE_MODE", "value", "false"),
            map("name", "NODE_ENV", "value", "${var.node_env}"),
            map("name", "NODE_HEAP_MB", "value", "2000"),
            map("name", "PORT", "value", "${var.web_port}"),
            map("name", "RECAPTCHA_SITE_KEY", "value", "6LcpAw4TAAAAAJAXwE3zk6PtyAzu6ORuI-QdAQ4D"),
            map("name", "RECAPTCHA_SECRET_KEY", "value", "6LcpAw4TAAAAAMLCYCIJ4ihKDR7_2NVclVyDJ0bH"),
            map("name", "SCAN_II_PUBLIC_KEY", "value", "${var.av_public_key}"),
            map("name", "SCAN_II_SECRET_KEY", "value", "${var.av_secret_key}"),
            map("name", "SERVICE_CREDS", "value", "${var.service_credentials}"),
            map("name", "SKIP_NEWRELIC", "value", "false"),
            map("name", "SMARTY_STREETS_AUTH_ID", "value", "${var.smarty_streets_auth_id}"),
            map("name", "SMARTY_STREETS_AUTH_TOKEN", "value", "${var.smarty_streets_auth_token}"),
            map("name", "SNS_TOPIC_ARN", "value", "${aws_sns_topic.hthu.arn}"),
            map("name", "SQS_QUEUE_URL", "value", "${aws_sqs_queue.web.id}"),
            map("name", "WEBPACK_ENTRY", "value", "${var.web_port}")
          )
        )
      )
    }"
  }
}

resource "aws_ecs_service" "web" {
  name            = "hthu-web"
  cluster         = "${module.ecs.cluster_id}"
  task_definition = "${aws_ecs_task_definition.web.arn}"
  desired_count   = "${var.default_replica_count}"
  launch_type     = "EC2"

  load_balancer {
    target_group_arn = "${aws_lb_target_group.web.id}"
    container_name   = "hthu-web"
    container_port   = "${var.web_port}"
  }

  lifecycle {
    ignore_changes = ["desired_count"]
  }
}

#################################################
#	Auto-Scaling
#################################################

resource "aws_appautoscaling_target" "web_scaling_target" {
  max_capacity       = "${var.scaling_max}"
  min_capacity       = "${var.scaling_min}"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.web.name}"
  role_arn           = "${data.aws_iam_role.this.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

/* resource "aws_appautoscaling_policy" "web_scaling_policy" {
  name               = "${module.ecs.cluster_id}-${aws_ecs_service.web.name}-scale-down"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "service/${module.ecs.cluster_id}/${aws_ecs_service.web.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  target_tracking_scaling_policy_configuration {
    target_value = "${var.cpu_scaling_target}"

    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }

  depends_on = ["aws_appautoscaling_target.web_scaling_target"]
} */

#################################################
#	Cloudwatch Log Group
#################################################

resource "aws_cloudwatch_log_group" "web" {
  name = "${var.env}-web"
  tags = "${var.tags}"
}

#################################################
#	Load Balancing
#################################################

resource "aws_lb_target_group" "web" {
  name        = "${var.env}-web"
  port        = "${var.web_port}"
  protocol    = "HTTP"
  vpc_id      = "${data.aws_vpc.core.id}"
  target_type = "instance"

  health_check {
    path = "/health"
  }
}

resource "aws_lb_listener" "hthu_80" {
  load_balancer_arn = "${aws_lb.hthu.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "hthu_443" {
  load_balancer_arn = "${aws_lb.hthu.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "${var.ssl_policy}"
  certificate_arn   = "${aws_acm_certificate.star_subdomain.arn}"

  default_action {
    target_group_arn = "${aws_lb_target_group.web.id}"
    type             = "forward"
  }
}

#################################################
#	DNS
#################################################

resource "aws_route53_record" "public_hthu_wildcard" {
  name    = "*.${var.subdomain_name}.${var.domain_name}"
  type    = "A"
  zone_id = "${data.aws_route53_zone.public.zone_id}"

  alias {
    name                   = "${aws_lb.hthu.dns_name}"
    zone_id                = "${aws_lb.hthu.zone_id}"
    evaluate_target_health = false
  }
}

#################################################
#	Messaging Bus
#################################################

resource "aws_sqs_queue" "web" {
  name = "${var.env}-web"
  tags = "${var.tags}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [{
    "Effect": "Allow",
    "Principal": {
      "AWS": "*"
    },
    "Action": "SQS:SendMessage",
    "Resource": "arn:aws:sqs:${var.region}:${data.aws_caller_identity.this.account_id}:${var.env}-web",
    "Condition": {
      "ArnEquals": {
        "aws:SourceArn": "${aws_sns_topic.hthu.arn}"
      }
    }
  }]
}
POLICY
}
