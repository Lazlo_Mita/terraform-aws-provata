module "ecs" {
  source   = "github.com/jjno91/terraform-aws-ecs-linux?ref=0.1.0"
  env      = "${var.env}"
  vpc_id   = "${data.aws_vpc.core.id}"
  min_size = "${var.ecs_min_size}"
  tags     = "${var.tags}"
}

#################################################
# Ingress
#################################################

resource "aws_security_group_rule" "ecs_ingress_alb" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  security_group_id        = "${module.ecs.sg_id}"
  source_security_group_id = "${aws_security_group.alb.id}"
}

resource "aws_security_group_rule" "ecs_ingress_provata_office" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = "${module.ecs.sg_id}"
  description       = "Provata Office"
  cidr_blocks       = ["50.225.121.230/32"]
}
