resource "aws_elasticache_cluster" "hthu" {
  # shortened name due to character limit
  cluster_id           = "hthu-${random_string.aws_elasticache_cluser_random_string.result}"
  engine               = "redis"
  engine_version       = "5.0.0"
  node_type            = "cache.t2.small"
  port                 = "${var.redis_port}"
  num_cache_nodes      = 1
  parameter_group_name = "default.redis5.0"
  subnet_group_name    = "${aws_elasticache_subnet_group.this.id}"
  security_group_ids   = ["${aws_security_group.redis.id}"]
  tags                 = "${var.tags}"
}

resource "aws_elasticache_cluster" "pvr" {
  cluster_id           = "pvr-${random_string.aws_elasticache_cluser_random_string.result}"
  engine               = "redis"
  engine_version       = "5.0.0"
  node_type            = "cache.t2.micro"
  port                 = "${var.redis_port}"
  num_cache_nodes      = 1
  parameter_group_name = "default.redis5.0"
  subnet_group_name    = "${aws_elasticache_subnet_group.this.id}"
  security_group_ids   = ["${aws_security_group.redis.id}"]
  tags                 = "${var.tags}"
}

resource "aws_elasticache_replication_group" "redis" {
  replication_group_id          = "${var.env}-redis-${random_string.aws_elasticache_cluser_random_string.result}"
  replication_group_description = "${var.env}-redis-${random_string.aws_elasticache_cluser_random_string.result}"
  node_type                     = "cache.t2.micro"
  port                          = "${var.redis_port}"
  automatic_failover_enabled    = true
  engine                        = "redis"
  engine_version                = "5.0.0"
  at_rest_encryption_enabled    = true
  transit_encryption_enabled    = true
  subnet_group_name             = "${aws_elasticache_subnet_group.this.id}"
  security_group_ids            = ["${aws_security_group.redis.id}"]
  tags                          = "${var.tags}"
  cluster_mode {
    replicas_per_node_group = "${var.redis_replicas_per_node_group}"
    num_node_groups         = "${var.redis_num_node_groups}"
  }
}

resource "random_string" "aws_elasticache_cluser_random_string" {
  length = 12
  special = false
  upper = false
}

resource "aws_elasticache_subnet_group" "this" {
  name       = "${var.env}"
  subnet_ids = ["${data.aws_subnet_ids.private.ids}"]
}

#################################################
# Security Group
#################################################

resource "aws_security_group" "redis" {
  name   = "${var.env}-redis"
  vpc_id = "${data.aws_vpc.core.id}"
  tags   = "${var.tags}"
}

#################################################
# Ingress
#################################################

resource "aws_security_group_rule" "redis_ingress_ecs" {
  type                     = "ingress"
  from_port                = "${var.redis_port}"
  to_port                  = "${var.redis_port}"
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.redis.id}"
  source_security_group_id = "${module.ecs.sg_id}"
}

