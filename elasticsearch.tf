resource "aws_elasticsearch_domain" "this" {
  domain_name           = "${var.env}"
  elasticsearch_version = "6.4"
  tags                  = "${var.tags}"

  cluster_config {
    instance_type  = "${var.elk_instance_type}"
    instance_count = "${var.elk_instance_count}"
  }

  ebs_options {
    ebs_enabled = "true"

    # total cluster size is volume_size times instance_count
    volume_size = "50"
  }

  vpc_options {
    security_group_ids = ["${aws_security_group.elk.id}"]
    subnet_ids         = ["${data.aws_subnet_ids.public.ids[0]}"]
  }

  encrypt_at_rest {
    enabled = "true"
  }

  node_to_node_encryption {
    enabled = "true"
  }
}

data "aws_iam_policy_document" "elasticsearch" {
  statement {
    principals = {
      type = "AWS"

      identifiers = [
        "${aws_iam_role.this.arn}",
      ]
    }

    actions = [
      "es:*",
    ]

    resources = [
      "${aws_elasticsearch_domain.this.arn}/*",
    ]
  }
}

resource "aws_elasticsearch_domain_policy" "this" {
  domain_name     = "${aws_elasticsearch_domain.this.domain_name}"
  access_policies = "${data.aws_iam_policy_document.elasticsearch.json}"
}

#################################################
# Security Group
#################################################

resource "aws_security_group" "elk" {
  name   = "${var.env}-elk"
  vpc_id = "${data.aws_vpc.core.id}"
  tags   = "${var.tags}"
}

#################################################
# Ingress
#################################################

resource "aws_security_group_rule" "elk_ingress_ecs" {
  type                     = "ingress"
  from_port                = "${var.elk_port}"
  to_port                  = "${var.elk_port}"
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.elk.id}"
  source_security_group_id = "${module.ecs.sg_id}"
}
