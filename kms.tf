resource "aws_kms_key" "this" {
  description = "Encrypt/decrypt key used for interservice messaging"
  tags        = "${merge(map("Name", var.env), var.tags)}"
}
