resource "aws_db_instance" "master" {
  identifier                = "${var.env}"
  final_snapshot_identifier = "${var.env}-final"
  allocated_storage         = 500
  storage_type              = "gp2"
  engine                    = "mysql"
  engine_version            = "5.7"
  instance_class            = "db.r4.xlarge"
  publicly_accessible       = true
  backup_retention_period   = "30"
  backup_window             = "09:30-10:30"
  username                  = "root"
  password                  = "${random_string.this.result}"
  storage_encrypted         = true
  db_subnet_group_name      = "${aws_db_subnet_group.this.id}"
  parameter_group_name      = "${aws_db_parameter_group.this.id}"
  vpc_security_group_ids    = ["${aws_security_group.rds.id}"]
  tags                      = "${var.tags}"
}

resource "aws_db_instance" "replica" {
  identifier             = "${var.env}-replica"
  skip_final_snapshot    = true
  replicate_source_db    = "${aws_db_instance.master.id}"
  instance_class         = "db.t2.medium"
  publicly_accessible    = true
  storage_encrypted      = true
  parameter_group_name   = "${aws_db_parameter_group.this.id}"
  vpc_security_group_ids = ["${aws_security_group.rds.id}"]
  tags                   = "${var.tags}"
}

resource "random_string" "this" {
  length           = 32
  override_special = "#-_+"
}

resource "aws_db_subnet_group" "this" {
  name_prefix = "${var.env}-"
  subnet_ids  = ["${data.aws_subnet_ids.public.ids}"]
  tags        = "${var.tags}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_db_parameter_group" "this" {
  name   = "${var.env}"
  family = "mysql5.7"
  tags   = "${var.tags}"

  parameter {
    name         = "lower_case_table_names"
    value        = "1"
    apply_method = "pending-reboot"
  }
}

#################################################
# Security Group
#################################################

resource "aws_security_group" "rds" {
  name   = "${var.env}-rds"
  vpc_id = "${data.aws_vpc.core.id}"
  tags   = "${var.tags}"
}

#################################################
# Ingress
#################################################

resource "aws_security_group_rule" "rds_ingress_ecs" {
  type                     = "ingress"
  from_port                = "${aws_db_instance.master.port}"
  to_port                  = "${aws_db_instance.master.port}"
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.rds.id}"
  source_security_group_id = "${module.ecs.sg_id}"
}

resource "aws_security_group_rule" "rds_ingress_self" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.rds.id}"
  self              = true
}

resource "aws_security_group_rule" "rds_ingress_hub" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.rds.id}"
  description       = "core-us-hub"
  cidr_blocks       = ["34.215.108.87/32", "52.88.172.65/32", "54.71.149.9/32"]
}

resource "aws_security_group_rule" "rds_ingress_provata_portland" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.rds.id}"
  description       = "Provata Office"
  cidr_blocks       = ["50.225.121.230/32"]
}

resource "aws_security_group_rule" "rds_ingress_legacy_vpn" {
  type              = "ingress"
  from_port         = "${aws_db_instance.master.port}"
  to_port           = "${aws_db_instance.master.port}"
  protocol          = "tcp"
  security_group_id = "${aws_security_group.rds.id}"
  description       = "eagan/legacy vpn"
  cidr_blocks       = ["97.65.93.224/28"]
}

#################################################
# Egress
#################################################

resource "aws_security_group_rule" "rds_egress_self" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.rds.id}"
  self              = true
}
