resource "aws_route53_zone" "private" {
  name = "${var.env}"
  tags = "${var.tags}"

  vpc {
    vpc_id = "${data.aws_vpc.core.id}"
  }
}

#################################################
# Public
#################################################

resource "aws_route53_record" "public_reports" {
  name    = "reports.${var.subdomain_name}.${var.domain_name}"
  type    = "CNAME"
  zone_id = "${data.aws_route53_zone.public.zone_id}"
  records = ["${aws_lb.reports.dns_name}"]
  ttl     = 300
}

#################################################
# Elasticsearch
#################################################

# had to be hard coded because aws_elasticsearch_domain doesn't provide a data object
resource "aws_route53_record" "private_elasticsearch" {
  name    = "elasticsearch"
  type    = "CNAME"
  zone_id = "${aws_route53_zone.private.zone_id}"
  records = ["vpc-core-canada-q2m5cl2hzxelrsuh3rb3zcgp3e.ca-central-1.es.amazonaws.com"]
  ttl     = 300
}

#################################################
# Redis
#################################################

resource "aws_route53_record" "private_redis" {
  name    = "redis"
  type    = "CNAME"
  zone_id = "${aws_route53_zone.private.zone_id}"
  records = ["${aws_elasticache_cluster.hthu.cache_nodes.0.address}"]
  ttl     = 300
}

resource "aws_route53_record" "private_redis_hthu" {
  name    = "redis.hthu"
  type    = "CNAME"
  zone_id = "${aws_route53_zone.private.zone_id}"
  records = ["${aws_elasticache_cluster.hthu.cache_nodes.0.address}"]
  ttl     = 300
}

#################################################
# RDS
#################################################

resource "aws_route53_record" "private_db_hthu" {
  name    = "db.hthu"
  type    = "CNAME"
  zone_id = "${aws_route53_zone.private.zone_id}"
  records = ["${aws_db_instance.master.address}"]
  ttl     = 300
}

resource "aws_route53_record" "private_readonly_db_hthu" {
  name    = "readonly.db.hthu"
  type    = "CNAME"
  zone_id = "${aws_route53_zone.private.zone_id}"
  records = ["${aws_db_instance.replica.address}"]
  ttl     = 300
}
