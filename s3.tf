resource "aws_s3_bucket" "logs" {
  bucket = "${var.env}-logs"
  acl    = "log-delivery-write"
  tags   = "${merge(local.s3_tags, var.tags)}"

  policy = <<POLICY
{
  "Id": "Policy",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:PutObject"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::${var.env}-logs/*",
      "Principal": {
        "AWS": [
          "${data.aws_elb_service_account.this.arn}"
        ]
      }
    }
  ]
}
POLICY

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket" "assets" {
  bucket = "${var.env}-assets"
  acl    = "private"
  tags   = "${merge(local.s3_tags, var.tags)}"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST", "GET"]
    allowed_origins = ["*.${var.domain_name}"]
    max_age_seconds = 3000
  }

  logging {
    target_bucket = "${aws_s3_bucket.logs.id}"
    target_prefix = "assets/"
  }
}

resource "aws_s3_bucket" "resources" {
  bucket = "${var.env}-resources"
  acl    = "private"
  tags   = "${merge(local.s3_tags, var.tags)}"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST", "GET"]
    allowed_origins = ["*.${var.domain_name}"]
    max_age_seconds = 3000
  }

  logging {
    target_bucket = "${aws_s3_bucket.logs.id}"
    target_prefix = "resources/"
  }
}

resource "aws_s3_bucket" "signups" {
  bucket = "${var.env}-signups"
  acl    = "private"
  tags   = "${merge(local.s3_tags, var.tags)}"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  logging {
    target_bucket = "${aws_s3_bucket.logs.id}"
    target_prefix = "signups/"
  }
}

resource "aws_s3_bucket" "certs" {
  bucket = "${var.env}-certs"
  acl    = "private"
  tags   = "${merge(local.s3_tags, var.tags)}"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  logging {
    target_bucket = "${aws_s3_bucket.logs.id}"
    target_prefix = "certs/"
  }
}

resource "aws_s3_bucket" "eligibility_files" {
  bucket = "${var.env}-eligibility-files"
  acl    = "private"
  tags   = "${merge(local.s3_tags, var.tags)}"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  logging {
    target_bucket = "${aws_s3_bucket.logs.id}"
    target_prefix = "eligibility-files/"
  }
}
