#################################################
#	AWS Secret Manager
#################################################

resource "random_string" "seed" {
  length           = 32
  override_special = "#-_+"
}

resource "aws_secretsmanager_secret" "this" {
  name = "${var.env}-external"
  tags = "${var.tags}"
}

resource "aws_secretsmanager_secret_version" "this" {
  secret_id = "${aws_secretsmanager_secret.this.id}"

  secret_string = "${jsonencode(map(
        "HUMAN_APP_KEY", "",
        "HUMAN_CLIENT_ID", "",
        "HUMAN_SECRET_KEY", "",
        "SHIPPING_FTP_HOST", "",
        "SHIPPING_FTP_PASSWORD", "",
        "SHIPPING_FTP_USERNAME", "",
        "MB_DB_HOST", "",
        "MB_DB_PASS", "",
        "HUMAN_API_TOKEN", "",
        "INTERCOM_APP_ID", "",
        "INTERCOM_SECRET", "",
        "RECAPTCHA_SECRET_KEY", "",
        "RECAPTCHA_SITE_KEY", "",
        "SCAN_II_PUBLIC_KEY", "",
        "SCAN_II_SECRET_KEY", "",
        "SERVICE_CREDS", "",
        "SMARTY_STREETS_AUTH_ID", "",
        "SMARTY_STREETS_AUTH_TOKEN", "",
        "HEALTHTAP_AUTHORIZATION", "",
        "HEALTHTAP_PROVATA_APP_ID", "",
        "HEALTHTAP_QUESTION_URL", "",
        "HEALTHTAP_INCOMING_TOKEN", "",
        "SFTP_PASSWORD", "",
        "SFTP_SECRET_ARN", "",
        "SFTP_USER", "",
        "JWT_TOKEN_SECRET", "",
        "SAML_CERT_PASSWORD", "",
        "AWS_ACCESS_KEY_ID", "",
        "HELP_SCOUT_API_TOKEN", "",
        "HELP_SCOUT_WEB_HOOK_SECRET_KEY", "",
        "ELIGIBILITY_EXPORT_PASSWORD", "",
        "ELIGIBILITY_IMPORT_NOTIFICATION_PASSWORD", "",
        "SEND_GRID_API_KEY", "",
        "FOND_API_KEY", "",
        "FOND_API_SECRET", "",
        "FOND_CALLBACK_SHARED_SECRET", "",
        "FOND_API_KEY_SANDBOX", "",
        "FOND_API_SECRET_SANDBOX", "",
        "FOND_CALLBACK_SHARED_SECRET_SANDBOX", "",
        "TANGO_PASSWORD", "",
        "BODYTRACE_INCOMING_AUTH_HEADER", ""
  ))}"

  # secret values are entered manaully and this prevents Terraform from overriding manual work
  lifecycle {
    ignore_changes = ["secret_string"]
  }
}

resource "aws_secretsmanager_secret" "internal" {
  name = "${var.env}-internal"
  tags = "${var.tags}"
}

resource "aws_secretsmanager_secret_version" "internal" {
  secret_id = "${aws_secretsmanager_secret.internal.id}"

  secret_string = "${jsonencode(map(
        "RDS_PASSWORD", "${random_string.this.result}",
        "SEED_PASSWORD", "${random_string.seed.result}"
  ))}"
}

