#################################################
# Topics
#################################################

resource "aws_sns_topic" "hthu" {
  name                                  = "${var.env}-hthu"
  application_success_feedback_role_arn = "${aws_iam_role.this.arn}"
  application_failure_feedback_role_arn = "${aws_iam_role.this.arn}"
  http_success_feedback_role_arn        = "${aws_iam_role.this.arn}"
  http_failure_feedback_role_arn        = "${aws_iam_role.this.arn}"
  lambda_success_feedback_role_arn      = "${aws_iam_role.this.arn}"
  lambda_failure_feedback_role_arn      = "${aws_iam_role.this.arn}"
  sqs_success_feedback_role_arn         = "${aws_iam_role.this.arn}"
  sqs_failure_feedback_role_arn         = "${aws_iam_role.this.arn}"
}

#################################################
# Subscriptions
#################################################

resource "aws_sns_topic_subscription" "hthu_audit" {
  topic_arn = "${aws_sns_topic.hthu.arn}"
  endpoint  = "${aws_sqs_queue.audit.arn}"
  protocol  = "sqs"
}

resource "aws_sns_topic_subscription" "hthu_billing" {
  topic_arn = "${aws_sns_topic.hthu.arn}"
  endpoint  = "${aws_sqs_queue.billing.arn}"
  protocol  = "sqs"
}

resource "aws_sns_topic_subscription" "hthu_challenge" {
  topic_arn = "${aws_sns_topic.hthu.arn}"
  endpoint  = "${aws_sqs_queue.challenge.arn}"
  protocol  = "sqs"
}

resource "aws_sns_topic_subscription" "hthu_core" {
  topic_arn = "${aws_sns_topic.hthu.arn}"
  endpoint  = "${aws_sqs_queue.core.arn}"
  protocol  = "sqs"
}

resource "aws_sns_topic_subscription" "hthu_data_science" {
  topic_arn = "${aws_sns_topic.hthu.arn}"
  endpoint  = "${aws_sqs_queue.data_science.arn}"
  protocol  = "sqs"
}

resource "aws_sns_topic_subscription" "hthu_eligibility" {
  topic_arn = "${aws_sns_topic.hthu.arn}"
  endpoint  = "${aws_sqs_queue.eligibility.arn}"
  protocol  = "sqs"
}

resource "aws_sns_topic_subscription" "hthu_email" {
  topic_arn = "${aws_sns_topic.hthu.arn}"
  endpoint  = "${aws_sqs_queue.email.arn}"
  protocol  = "sqs"
}

resource "aws_sns_topic_subscription" "hthu_integration" {
  topic_arn = "${aws_sns_topic.hthu.arn}"
  endpoint  = "${aws_sqs_queue.integration.arn}"
  protocol  = "sqs"
}

resource "aws_sns_topic_subscription" "hthu_native" {
  topic_arn = "${aws_sns_topic.hthu.arn}"
  endpoint  = "${aws_sqs_queue.native.arn}"
  protocol  = "sqs"
}

resource "aws_sns_topic_subscription" "hthu_progress" {
  topic_arn = "${aws_sns_topic.hthu.arn}"
  endpoint  = "${aws_sqs_queue.progress.arn}"
  protocol  = "sqs"
}

resource "aws_sns_topic_subscription" "hthu_supplies" {
  topic_arn = "${aws_sns_topic.hthu.arn}"
  endpoint  = "${aws_sqs_queue.supplies.arn}"
  protocol  = "sqs"
}

resource "aws_sns_topic_subscription" "hthu_vitals" {
  topic_arn = "${aws_sns_topic.hthu.arn}"
  endpoint  = "${aws_sqs_queue.vitals.arn}"
  protocol  = "sqs"
}

resource "aws_sns_topic_subscription" "hthu_web" {
  topic_arn = "${aws_sns_topic.hthu.arn}"
  endpoint  = "${aws_sqs_queue.web.arn}"
  protocol  = "sqs"
}
