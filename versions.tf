#################################################
# Be aware that this file is updated regularly through automation housed in the service
# repositories. Make updates with care as the map keys are expected to have a 1:1
# coorelation with the service repository names.
#
# Service repositories can be found here:
# https://bitbucket.org/account/user/staywell_core/projects/SPECS
#################################################

variable "versions" {
  type = "map"

  default = {
    hthu-auth           = "b6ea2f4"
    hthu-billing        = "e5c40f1"
    hthu-challenge      = "cee23df"
    hthu-core           = "16ff8a18"
    hthu-data-science   = "a4007fe"
    hthu-eligibility    = "7012d25"
    hthu-email          = "b6535cf"
    hthu-integration    = "0ca2d57"
    hthu-native-service = "c89c73d"
    hthu-progress       = "f00f712"
    pvr-service         = "e6db8fb"
    metabase            = "v0.31.2"
    hthu-supplies       = "27b448f"
    hthu-vitals         = "ec8fba3"
    hthu-web            = "37ccef2d2"
  }
}
